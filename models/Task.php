<?php

namespace app\models;

use app\components\FcmPusher;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "task".
 *
 * @property int $id
 * @property int $petition_id Обращение
 * @property int $house_id Дом
 * @property int $executor_id Исполнитель
 * @property int $status_id Статус
 * @property string $execute_datetime Срок исполнения
 * @property string $description Описание
 * @property string $description_short Краткое описание
 * @property int $priority Приоритет
 * @property string $created_at Дата и время создания
 * @property int $is_fee Платно
 * @property int $created_by Создатель
 * @property int $child_task_id Подзадача
 * @property int $parent_task_id Родительская задача
 *
 * @property Task $childTask
 * @property Users $createdBy
 * @property Users $executor
 * @property House $house
 * @property Petition $petition
 * @property Status $status
 */
class Task extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => null,
                'createdAtAttribute' => 'created_at',
                'value' => date('Y-m-d H:i:s'),
            ],
            [
                'class' => BlameableBehavior::class,
                'updatedByAttribute' => null,
                'createdByAttribute' => 'created_by',
                'value' => Yii::$app->user->getId(),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['house_id', 'description', 'executor_id'], 'required'],
            [['petition_id', 'house_id', 'executor_id', 'status_id', 'priority', 'created_by', 'is_fee', 'parent_task_id'], 'integer'],
            [['execute_datetime', 'created_at'], 'safe'],
            [['description', 'description_short'], 'string'],
            [['child_task_id'], 'exist', 'skipOnError' => true, 'targetClass' => Task::className(), 'targetAttribute' => ['child_task_id' => 'id']],
            [['executor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['executor_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['house_id'], 'exist', 'skipOnError' => true, 'targetClass' => House::className(), 'targetAttribute' => ['house_id' => 'id']],
            [['petition_id'], 'exist', 'skipOnError' => true, 'targetClass' => Petition::className(), 'targetAttribute' => ['petition_id' => 'id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'petition_id' => 'Обращение',
            'house_id' => 'Дом',
            'executor_id' => 'Исполнитель',
            'status_id' => 'Статус',
            'company_id' => 'Компания',
            'is_fee' => 'Платно',
            'execute_datetime' => 'Срок исполнения',
            'description' => 'Описание',
            'description_short' => 'Краткое описание',
            'priority' => 'Приоритет',
            'created_at' => 'Дата и время создания',
            'created_by' => 'Создатель',
            'child_task_id' => 'Подзадача',
            'parent_task_id' => 'Родительская зачада',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function beforeValidate()
    {
        // if($this->house_id == '0'){
        //     $this->house_id = null;
        //     $parent = self::findOne($this->house_id);
        //     if($parent){
        //         $this->house_id = $parent->house_id;
        //     }
        // }

        // if($this->status_id == '0'){
        //     $this->status_id = null;
        // }


        return parent::beforeValidate();
    }

    /**
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        if($this->isNewRecord){
            $this->company_id = Yii::$app->user->identity->company_id;
        }

        return parent::beforeSave($insert);
    }

    /**
     * {@inheritdoc}
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if($insert && $this->executor_id != null){
            $executor = $this->executor;
            FcmPusher::push($executor->fcm_token, "Новая задача «{$this->description_short}»");
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(Users::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChildTask()
    {
        return $this->hasOne(Task::className(), ['id' => 'child_task_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHouse()
    {
        return $this->hasOne(House::className(), ['id' => 'house_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPetition()
    {
        return $this->hasOne(Petition::className(), ['id' => 'petition_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExecutor()
    {
        return $this->hasOne(Users::className(), ['id' => 'executor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }
}
