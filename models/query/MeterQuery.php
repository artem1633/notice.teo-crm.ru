<?php

namespace app\models\query;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\app\models\Meter]].
 *
 * @see \app\models\Meter
 */
class MeterQuery extends ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * Выборка для помещения
     * @param int $id Идентификатор помещения
     * @return $this
     */
    public function forApartment($id)
    {
        return $this->andWhere(['apartment_id' => $id]);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\Meter[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\Meter|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
