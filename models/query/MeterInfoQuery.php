<?php

namespace app\models\query;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\app\models\MeterInfo]].
 *
 * @see \app\models\MeterInfo
 */
class MeterInfoQuery extends ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * Для счетчика
     * @param int $id Идентификатор счетчика
     * @return $this
     */
    public function forMeter($id)
    {
        return $this->andWhere(['meter_id' => $id]);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\MeterInfo[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\MeterInfo|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
