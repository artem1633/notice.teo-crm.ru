<?php

namespace app\models\query;

use app\models\Users;

/**
 * This is the ActiveQuery class for [[\app\models\StatusColor]].
 *
 * @see \app\models\StatusColor
 */
class StatusColorQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * Для компании
     * @return $this
     */
    public function forCompany()
    {
        /** @var Users $identity */
        $identity = \Yii::$app->user->identity;
        return $this->andWhere(['company_id' => $identity->company_id]);
    }

    /**
     * Для статуса
     * @param int $id Идентификатор статуса
     * @return $this
     */
    public function forStatus($id)
    {
        return $this->andWhere(['status_id' => $id]);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\StatusColor[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\StatusColor|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
