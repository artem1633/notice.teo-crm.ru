<?php

namespace app\models;


/**
 * This is the model class for table "apartment_meter".
 *
 * @property int $id
 * @property int $apartment_id Помещение
 * @property double $cold_water Холодная вода
 * @property double $hot_water Холодная вода
 * @property double $heat Отопление
 * @property double $gas Газ
 * @property double $electricity Электроэнергия
 * @property string $meter_number Номер прибора учета
 * @property string $next_date_check Дата следующей проверки
 * @property string $created_at Дата предоставления
 *
 * @property Apartment $apartment
 */
class ApartmentMeter extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'apartment_meter';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['apartment_id'], 'integer'],
            [['cold_water', 'hot_water', 'heat', 'gas', 'electricity'], 'number'],
            [['next_date_check', 'created_at'], 'safe'],
            [['meter_number'], 'string', 'max' => 255],
            [['apartment_id'], 'exist', 'skipOnError' => true, 'targetClass' => Apartment::className(), 'targetAttribute' => ['apartment_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'apartment_id' => 'Помещение',
            'cold_water' => 'Холодная вода',
            'hot_water' => 'Горячая вода',
            'heat' => 'Отопление',
            'gas' => 'Газ',
            'electricity' => 'Электроэнергия',
            'meter_number' => 'Номер прибора учета',
            'next_date_check' => 'Дата следующей проверки',
            'created_at' => 'Дата',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApartment()
    {
        return $this->hasOne(Apartment::className(), ['id' => 'apartment_id']);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\query\ApartmentMeterQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\ApartmentMeterQuery(get_called_class());
    }
}
