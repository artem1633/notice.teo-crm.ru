<?php

namespace app\models;

/**
 * This is the model class for table "house_meter".
 *
 * @property int $id
 * @property int $house_id Помещение
 * @property double $cold_water Холодная вода
 * @property double $hot_water Холодная вода
 * @property double $heat Отопление
 * @property double $gas Газ
 * @property double $electricity Электроэнергия
 * @property string $created_at Дата предоставления
 *
 * @property House $house
 */
class HouseMeter extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'house_meter';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['house_id'], 'integer'],
            [['cold_water', 'hot_water', 'heat', 'gas', 'electricity'], 'number'],
            [['created_at'], 'safe'],
            [['house_id'], 'exist', 'skipOnError' => true, 'targetClass' => House::className(), 'targetAttribute' => ['house_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'house_id' => 'Дом',
            'cold_water' => 'Холодная вода',
            'hot_water' => 'Горячая вода',
            'heat' => 'Отопление',
            'gas' => 'Газ',
            'electricity' => 'Электроэнергия',
            'created_at' => 'Дата',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHouse()
    {
        return $this->hasOne(House::className(), ['id' => 'house_id']);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\query\HouseMeterQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\HouseMeterQuery(get_called_class());
    }
}
