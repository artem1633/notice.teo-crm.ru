<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "task_attachment".
 *
 * @property int $id
 * @property int $task_id Задача
 * @property string $param1 Параметр
 * @property string $param2 Параметр 2
 * @property int $type Тип
 * @property int $user_id Пользователь
 * @property string $created_at Дата и время добавления
 *
 * @property Task $task
 * @property Users $user
 */
class TaskAttachment extends \yii\db\ActiveRecord
{
    const TYPE_COMMENT = 0;
    const TYPE_IMAGE = 1;
    const TYPE_SOUND = 2;
    const TYPE_HISTORY = 3;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_attachment';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => null,
                'createdAtAttribute' => 'created_at',
                'value' => date('Y-m-d H:i:s'),
            ],
            [
                'class' => BlameableBehavior::class,
                'updatedByAttribute' => null,
                'createdByAttribute' => 'user_id',
                'value' => Yii::$app->user->getId(),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['task_id', 'type', 'user_id'], 'integer'],
            [['created_at'], 'safe'],
            [['param1', 'param2'], 'string', 'max' => 255],
            [['task_id'], 'exist', 'skipOnError' => true, 'targetClass' => Task::className(), 'targetAttribute' => ['task_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'task_id' => 'Задача',
            'param1' => 'Параметр',
            'param2' => 'Параметр 2',
            'type' => 'Тип',
            'user_id' => 'Пользователь',
            'created_at' => 'Дата и время добавления',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::className(), ['id' => 'task_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }
}
