<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "crash_disable".
 *
 * @property int $id
 * @property int $house_id Дом
 * @property string $start_datetime Дата и время начала отключения
 * @property string $plan_end_datetime Планируемая дата и время окончания
 * @property string $fact_end_datetime Фактическая дата и время окончания
 * @property string $resource Ресурс
 * @property string $status Статус
 * @property int $company_id Компания
 * @property string $created_at Дата и время создания
 * @property int $created_by Создатель
 *
 * @property Company $company
 * @property Users $createdBy
 * @property House $house
 */
class CrashDisable extends \yii\db\ActiveRecord
{
    const RESOURCE_GAZ = 0;
    const RESOURCE_WATER_COLD = 1;
    const RESOURCE_POWER = 2;
    const RESOURCE_WARM = 3;
    const RESOURCE_WATER_HOT = 4;

    const STATUS_NEW = 0;
    const STATUS_WORK = 1;
    const STATUS_DONE = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'crash_disable';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => null,
                'createdAtAttribute' => 'created_at',
                'value' => date('Y-m-d H:i:s'),
            ],
            [
                'class' => BlameableBehavior::class,
                'updatedByAttribute' => null,
                'createdByAttribute' => 'created_by',
                'value' => Yii::$app->user->getId(),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['house_id', 'company_id', 'created_by'], 'integer'],
            [['start_datetime', 'plan_end_datetime', 'fact_end_datetime', 'created_at', 'comment'], 'safe'],
            [['resource', 'status'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['house_id'], 'exist', 'skipOnError' => true, 'targetClass' => House::className(), 'targetAttribute' => ['house_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'house_id' => 'Дом',
            'start_datetime' => 'Дата и время начала отключения',
            'plan_end_datetime' => 'Планируемая дата и время окончания',
            'fact_end_datetime' => 'Фактическая дата и время окончания',
            'comment' => 'Комментарий',
            'resource' => 'Ресурс',
            'status' => 'Статус',
            'company_id' => 'Компания',
            'created_at' => 'Дата и время создания',
            'created_by' => 'Создатель',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        if($this->isNewRecord){
            $this->company_id = Yii::$app->user->identity->company_id;
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return array
     */
    public static function resourceLabels()
    {
        return [
            self::RESOURCE_GAZ => 'Газ',
            self::RESOURCE_WATER_COLD => 'Холодная Вода',
            self::RESOURCE_WATER_HOT => 'Горячая Вода',
            self::RESOURCE_POWER => 'Электричество',
            self::RESOURCE_WARM => 'Отопление',
        ];
    }

    /**
     * @return array
     */
    public static function statusLabels()
    {
        return [
            self::STATUS_NEW => 'Новое',
            self::STATUS_WORK => 'В работе',
            self::STATUS_DONE => 'Ликвидировано',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(Users::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHouse()
    {
        return $this->hasOne(House::className(), ['id' => 'house_id']);
    }
}
