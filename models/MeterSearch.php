<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * MeterSearch represents the model behind the search form of `app\models\Meter`.
 */
class MeterSearch extends Meter
{
    public $address;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'apartment_id', 'type'], 'integer'],
            [['number', 'note', 'date_verify', 'address'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $asArray = false, $api = false)
    {
        $query = Meter::find()->select('meter.*, house.address as house_address, meter_info.created_at, meter_info.updated_at, meter_info.value, meter_info.file_path');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if($api){
            $this->load($params, 'model');
        } else {
            $this->load($params);
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->leftJoin('apartment', 'apartment.id=meter.apartment_id');
        $query->leftJoin('house', 'house.id=apartment.house_id');
        $query->leftJoin('meter_info', 'meter_info.meter_id=meter.id');

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'apartment_id' => $this->apartment_id,
            'type' => $this->type,
            'date_verify' => $this->date_verify,
        ]);

        $query->andFilterWhere(['like', 'number', $this->number])
            ->andFilterWhere(['like', 'house.address', $this->address])
            ->andFilterWhere(['like', 'note', $this->note]);

        $query->andWhere(['house.company_id' => Yii::$app->user->identity->company_id]);

        if($asArray){
            $query->asArray();
        }

        $query->groupBy('meter.id');

        return $dataProvider;
    }
}
