<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MeterInfo;

/**
 * MeterInfoSearch represents the model behind the search form about `app\models\MeterInfo`.
 */
class MeterInfoSearch extends MeterInfo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'meter_id'], 'integer'],
            [['created_at'], 'safe'],
            [['value'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $asArray = false, $api = false)
    {
        $query = MeterInfo::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if($api){
            $this->load($params, 'model');
        } else {
            $this->load($params);
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->leftJoin('meter', 'meter.id=meter_info.meter_id');
        $query->leftJoin('apartment', 'apartment.id=meter.apartment_id');
        $query->leftJoin('house', 'house.id=apartment.house_id');

        $query->andFilterWhere([
            'meter_info.id' => $this->id,
            'meter_id' => $this->meter_id,
            'meter_info.created_at' => $this->created_at,
            'value' => $this->value,
        ]);

        $query->andWhere(['house.company_id' => Yii::$app->user->identity->company_id]);

        if($asArray){
            $query->asArray();
        }

        $query->groupBy('meter_info.id');

        return $dataProvider;
    }
}
