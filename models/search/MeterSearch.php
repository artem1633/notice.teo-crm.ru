<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Meter;

/**
 * MeterSearch represents the model behind the search form about `app\models\Meter`.
 */
class MeterSearch extends Meter
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'apartment_id', 'type'], 'integer'],
            [['number', 'note'], 'safe'],
            [['date_start', 'date_end'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Meter::find();
        $query->joinWith(['meterInfos']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'apartment_id' => $this->apartment_id,
            'type' => $this->type,
        ]);

        $query->andFilterWhere(['like', 'number', $this->number])
            ->andFilterWhere(['like', 'note', $this->note]);

        if (!$this->date_start && !$this->date_end){
            $this->date_start = date('Y-m-01', time());
            $this->date_end = date('Y-m-d', time());
        }

        if ($this->date_start && !$this->date_end){
            $this->date_end = date('Y-m-d', time());
        }

        if ($this->date_start && $this->date_end){
            $this->date_start = date('Y-m-d', strtotime($this->date_start));
            $this->date_end = date('Y-m-d', strtotime($this->date_end));
            $query->andFilterWhere(['BETWEEN', 'meter_info.created_at', $this->date_start, $this->date_end]);
        }

        return $dataProvider;
    }
}
