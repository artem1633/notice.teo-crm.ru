<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\HouseMeter;

/**
 * HouseMeterSearch represents the model behind the search form about `app\models\HouseMeter`.
 */
class HouseMeterSearch extends HouseMeter
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'house_id'], 'integer'],
            [['cold_water', 'hot_water', 'heat', 'gas', 'electricity'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = HouseMeter::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query->orderBy(['created_at' => SORT_DESC]),
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'house_id' => $this->house_id,
            'cold_water' => $this->cold_water,
            'hot_water' => $this->hot_water,
            'heat' => $this->heat,
            'gas' => $this->gas,
            'electricity' => $this->electricity,
        ]);

        return $dataProvider;
    }
}
