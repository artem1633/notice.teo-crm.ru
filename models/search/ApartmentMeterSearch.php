<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ApartmentMeter;

/**
 * ApartmentMeterSearch represents the model behind the search form about `app\models\ApartmentMeter`.
 */
class ApartmentMeterSearch extends ApartmentMeter
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'apartment_id'], 'integer'],
            [['cold_water', 'hot_water', 'heat', 'gas', 'electricity'], 'number'],
            [['meter_number', 'next_date_check', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ApartmentMeter::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'apartment_id' => $this->apartment_id,
            'cold_water' => $this->cold_water,
            'hot_water' => $this->hot_water,
            'heat' => $this->heat,
            'gas' => $this->gas,
            'electricity' => $this->electricity,
            'next_date_check' => $this->next_date_check,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'meter_number', $this->meter_number]);

        return $dataProvider;
    }
}
