<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CrashDisable;
use yii\data\ArrayDataProvider;

/**
 * CrashDisableSearch represents the model behind the search form of `app\models\CrashDisable`.
 */
class CrashDisableSearch extends CrashDisable
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'house_id', 'company_id', 'created_by'], 'integer'],
            [['start_datetime', 'plan_end_datetime', 'fact_end_datetime', 'resource', 'status', 'created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $asArray = false, $api = false)
    {
        $query = CrashDisable::find();

        // add conditions that should always apply here

        if($api){
            $dataProvider = new ArrayDataProvider();
            $this->load($params, 'model');
        } else {
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
            ]);
            $this->load($params);
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'house_id' => $this->house_id,
            'start_datetime' => $this->start_datetime,
            'plan_end_datetime' => $this->plan_end_datetime,
            'fact_end_datetime' => $this->fact_end_datetime,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
        ]);

        $query->andFilterWhere(['like', 'resource', $this->resource])
            ->andFilterWhere(['like', 'status', $this->status]);

        $query->andFilterWhere(['company_id' => Yii::$app->user->identity->company_id]);

        if($asArray){
            $query->asArray();
        }

        if($api){
            $query->asArray();

            $models = $query->all();

            for ($i = 0; $i < count($models); $i++)
            {
                $model = $models[$i];

                if($model['house_id'] != null){
                    $house = House::findOne($model['house_id']);
                    if($house){
                        $models[$i]['house'] = $house;
                    }
                }

                if($model['created_by'] != null){
                    $user = Users::findOne($model['created_by']);
                    if($user){
                        $models[$i]['createdBy'] = $user;
                    }
                }
            }

            $dataProvider->models = $models;
        }

        return $dataProvider;
    }
}
