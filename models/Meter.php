<?php

namespace app\models;

use app\models\query\MeterQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "meter".
 *
 * @property int $id
 * @property int $apartment_id Помещение
 * @property string $number Номер прибора
 * @property int $type Тип счетчика (газ, вода и пр.)
 * @property string $note Примечание
 * @property string $typeLabel Наименование типа счетчика
 * @property string $date_verify Дата проверки
 * @property string $date_start Дата начала периода (показ истрии)
 * @property string $date_end Дата конца периода (показ истории)
 *
 * @property Apartment $apartment
 * @property MeterInfo[] $meterInfos
 */
class Meter extends ActiveRecord
{
    const METER_TYPE_HOT_WATER = 1;
    const METER_TYPE_COLD_WATER = 2;
    const METER_TYPE_GAS = 3;
    const METER_TYPE_HEAT = 4;
    const METER_TYPE_ELECTRICITY = 5;

    public $date_start;
    public $date_end;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'meter';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['apartment_id', 'type'], 'integer'],
            [['note'], 'string'],
            [['number'], 'string', 'max' => 255],
            [
                ['apartment_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Apartment::className(),
                'targetAttribute' => ['apartment_id' => 'id']
            ],
            ['date_verify', 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'apartment_id' => 'Помещение',
            'number' => 'Номер прибора',
            'type' => 'Что учитывает прибор',
            'note' => 'Примечание',
            'date_verify' => 'Дата проверки',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApartment()
    {
        return $this->hasOne(Apartment::className(), ['id' => 'apartment_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeterInfos()
    {
        return $this->hasMany(MeterInfo::className(), ['meter_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return MeterQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MeterQuery(get_called_class());
    }

    /**
     * Список типов счетчика
     * @return array
     */
    public function getTypeList()
    {
        return [
            self::METER_TYPE_HOT_WATER => 'Горячая вода',
            self::METER_TYPE_COLD_WATER => 'Холодная вода',
            self::METER_TYPE_GAS => 'Газ',
            self::METER_TYPE_HEAT => 'Отопление',
            self::METER_TYPE_ELECTRICITY => 'Электричество',
        ];
    }

    /**
     * Получает и возвращает наименование типа счетчика
     * @return mixed
     */
    public function getTypeLabel()
    {
        return $this->getTypeList()[$this->type];
    }

    public function getValueByDate($date)
    {
        return MeterInfo::find()->andWhere(['meter_id' => $this->id, 'created_at' => $date])->one()->value;
    }

    /**
     * Возвращает последнее значение счетчика
     * @return float|int
     */
    public function getLastValue()
    {
        $meter_info = MeterInfo::find()->forMeter($this->id)->orderBy(['id' => SORT_DESC])->one();

        if ($meter_info) {
            return $meter_info->value;
        }

        return 0;
    }


}
