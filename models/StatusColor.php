<?php

namespace app\models;

use app\models\query\StatusColorQuery;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "status_color".
 *
 * @property int $id
 * @property int $status_id Статус
 * @property string $color Цвет
 * @property int $company_id Компания
 * @property array $status_petition Статусы петиции
 *
 * @property Company $company
 * @property Status $status
 */
class StatusColor extends ActiveRecord
{
    public $status_petition;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'status_color';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status_id', 'company_id'], 'integer'],
            [['color'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status_id' => 'id']],
            ['status_petition', 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status_id' => 'Статус',
            'color' => 'Цвет',
            'company_id' => 'Компания',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * {@inheritdoc}
     * @return StatusColorQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new StatusColorQuery(get_called_class());
    }

    public function getList()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }

    /**
     * Получает цвет статуса
     * @param int $status_id Идентификатор статуса
     * @return string
     */
    public function getColor($status_id)
    {
        $color = StatusColor::findOne(['status_id' => $status_id, 'company_id' => Yii::$app->user->identity->company_id]);
        if ($color){
            return $color->color;
        }
        return null;

    }

    /**
     * Возвращает цвет для статуса обращения
     * @param int $status_id Идентификатор статуса
     * @return null|string
     */
    public static function getColorForStatus($status_id)
    {
        return StatusColor::find()->forCompany()->forStatus($status_id)->one()->color ?? null;
    }
}
