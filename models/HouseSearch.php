<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * HouseSearch represents the model behind the search form of `app\models\House`.
 */
class HouseSearch extends House
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'street_id', 'residential_number', 'non_residential_number', 'document_id', 'company_id'], 'integer'],
            [['fias_number', 'cadastral_number', 'additional_info', 'number', 'address', 'import_address'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $api = false)
    {
        $query = House::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if($api){
            $this->load($params, 'model');
        } else {
            $this->load($params);
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'street_id' => $this->street_id,
            'residential_number' => $this->residential_number,
            'non_residential_number' => $this->non_residential_number,
            'document_id' => $this->document_id,
        ]);

        $query->andFilterWhere(['like', 'fias_number', $this->fias_number])
            ->andFilterWhere(['like', 'cadastral_number', $this->cadastral_number])
            ->andFilterWhere(['like', 'additional_info', $this->additional_info])
            ->andFilterWhere(['like', 'number', $this->number])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'import_address', $this->import_address]);

        $query->andFilterWhere(['company_id' => Yii::$app->user->identity->company_id]);

        return $dataProvider;
    }
}
