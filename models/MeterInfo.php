<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

/**
 * This is the model class for table "meter_info".
 *
 * @property int $id
 * @property int $meter_id Прибор учета
 * @property string $created_at Дата сведеений
 * @property string $updated_at Дата изменения сведеений
 * @property double $value Показания (значение прибора учета)
 * @property int $apartment_id Идентификатор помещения
 * @property string $file_path Файл
 * @property array $meters_info Показания счетчиков
 * @property array $prev_month_meters_info Показания счетчиков за предыдущий месяц
 *
 * @property UploadedFile $file Файл
 *
 * @property Meter $meter
 * @property Apartment $apartment
 * @property MeterInfo[] $values
 */
class MeterInfo extends ActiveRecord
{
    public $apartment_id;
    public $meters_info;
    public $prev_month_meters_info;

    /** @var UploadedFile */
    public $file;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'meter_info';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['meter_id', 'apartment_id'], 'integer'],
            [['created_at', 'updated_at', 'meters_info', 'file_path'], 'safe'],
            [['value'], 'number'],
            [
                ['meter_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Meter::className(),
                'targetAttribute' => ['meter_id' => 'id']
            ],
            ['file', 'file'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'meter_id' => 'Прибор учета',
            'created_at' => 'Дата сведений',
            'updated_at' => 'Дата изменения сведений',
            'value' => 'Показания',
            'apartment_id' => 'Помещение',
            'file_path' => 'Фото',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        if (!$this->created_at) {
            $this->created_at = date('Y-m-d 00:00:00', time());
        } else {
            $this->created_at = date('Y-m-d H:i:s', strtotime($this->created_at));
        }

        $this->file = UploadedFile::getInstanceByName('file');
        if($this->file){
            // $this->file = UploadedFile::getInstance($this, 'file');
            // $this->file = UploadedFile::getInstanceByName('file');

            \Yii::warning($this->file, 'file uploaded');

            if(is_dir('uploads') == false){
                mkdir('uploads');
            }

            $path = "uploads/".Yii::$app->security->generateRandomString().".".$this->file->extension;

            $this->file->saveAs($path);
            $this->file_path = $path;
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeter()
    {
        return $this->hasOne(Meter::className(), ['id' => 'meter_id']);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\query\MeterInfoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\MeterInfoQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApartment()
    {
        return $this->hasOne(Apartment::class, ['id' => 'apartment_id'])
            ->via('meter');
    }

    /**
     * Возвращает массив счетчиков для помещения
     * @return array
     */
    public function getListMeter()
    {
        $query = Meter::find()
            ->forApartment($this->apartment_id);

        $list = [];

        /** @var Meter $meter */
        foreach ($query->each() as $meter) {
            $list[$meter->id] = $meter->typeLabel . "({$meter->number}, {$meter->note})";
        }

        return $list;
    }

    /**
     * @param int $meter_id Идентификатор счетчика
     * @param string $date_start
     * @param string $date_end
     * @return MeterInfo[]|array
     */
    public function getValues($meter_id = null, $date_start = null, $date_end = null)
    {
        if (!$date_start || !$date_end) {
            return self::find()->forMeter($meter_id)->all();
        } else {
            return self::find()->forMeter($meter_id)->andWhere([
                'BETWEEN',
                'created_at',
                $date_start,
                $date_end
            ])->all();
        }
    }

    /**
     * Возвращает предыдущее значение счетчика
     */
    public function getPrevValue()
    {
        $meter_info = self::find()->forMeter($this->meter_id)->andWhere([
            '<',
            'created_at',
            $this->created_at
        ])->orderBy(['created_at' => SORT_DESC])->one();

        if ($meter_info){
            return $meter_info->value;
        }

        return null;
    }
}
