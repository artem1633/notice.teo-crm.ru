<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Task;
use yii\data\ArrayDataProvider;

/**
 * TaskSearch represents the model behind the search form of `app\models\Task`.
 */
class TaskSearch extends Task
{
    public $address;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'petition_id', 'house_id', 'executor_id', 'status_id', 'priority', 'created_by', 'is_fee', 'company_id'], 'integer'],
            [['execute_datetime', 'description', 'description_short', 'created_at', 'address'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $asArray = false, $api = false)
    {
        $query = Task::find()->select('task.*, house.address as house_address');

        if($api){
            $dataProvider = new ArrayDataProvider();

            $this->load($params, 'model');
        } else {
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
            ]);

            $this->load($params);
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->leftJoin('house', 'house.id=task.house_id');

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'petition_id' => $this->petition_id,
            'house_id' => $this->house_id,
            'executor_id' => $this->executor_id,
            'status_id' => $this->status_id,
            'execute_datetime' => $this->execute_datetime,
            'priority' => $this->priority,
            'is_fee' => $this->is_fee,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'house.address', $this->address])
            ->andFilterWhere(['like', 'description_short', $this->description_short]);

        $query->andFilterWhere(['task.company_id' => Yii::$app->user->identity->company_id]);

//        if($api == false){
//            $query->andFilterWhere(['created_by' => Yii::$app->user->getId()]);
//        }

        if($asArray){
            $query->asArray();
        }


        if(Yii::$app->user->identity->permission == Users::USER_ROLE_SPECIALIST){
            $query->andWhere(['executor_id' => Yii::$app->user->getId()]);
        }

        $query->groupBy('task.id');

        if($api){

            $models = $query->asArray()->all();

            for ($i = 0; $i < count($models); $i++)
            {
                $house = House::findOne($models[$i]['house_id']);
                $petition = Petition::findOne($models[$i]['petition_id']);
                $executor = Users::findOne($models[$i]['executor_id']);
                $status = Status::findOne($models[$i]['status_id']);

                $models[$i]['house'] = $house;
                $models[$i]['petition'] = $petition;
                $models[$i]['executor'] = $executor;
                $models[$i]['status'] = $status;
            }

            $dataProvider->models = $models;
        }

        return $dataProvider;
    }
}
