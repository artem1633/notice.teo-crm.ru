<?php

namespace app\models;

/**
 * This is the model class for table "knowledge_base".
 *
 * @property int $id
 * @property string $name Заголовок
 * @property string $content Содержание
 */
class KnowledgeBase extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'knowledge_base';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content'], 'string'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Заголовок',
            'content' => 'Содержание',
        ];
    }
}
