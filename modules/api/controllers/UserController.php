<?php

namespace app\modules\api\controllers;

use app\models\User;
use app\models\Users;
use app\modules\api\models\AvatarUploadForm;
use app\modules\api\models\LoginForm;
use Yii;
use yii\web\Response;
use app\components\FcmPusher;
use yii\helpers\ArrayHelper;

/**
 * Class UserController
 * @package app\modules\api\controllers
 */
class UserController extends BaseController
{
    /**
     *
     */
    public function actionExecutors()
    {
        $users = Users::find()->where(['permission' => Users::USER_ROLE_SPECIALIST])->all();

        return $users;
    }

    /**
     * POST
     */
    public function actionSetFcmToken()
    {
        $token = $_POST['fcm_token'];

        Yii::$app->user->identity->fcm_token = $token;
        $result = Yii::$app->user->identity->save(false);

        return ['result' => $result];
    }

    public function actionTestPush()
    {
        // $response = FcmPusher::push('cJzLQBXHTKWR9wL4aU4VfZ%3AAPA91bFcf-0IbKuLwteFIn5D7qyzSmahzXR2fB82N4MlZ_w5f-ckHjBQ7pg0GBa85G6tmuvU7tWiT1se_fDJqu1s1bRMHLJ7Cp_7OeCFpg58zTaditlNJd0dNO77q3T91By6dhdIlBJw', 'Привет');

        $url = 'https://fcm.googleapis.com/fcm/send';

        $request_body = [
            'to' => 'c-yjbJxqTROs9Y9rPjfrJE:APA91bHcde_FQTMMExcQn8sMn9sN1Oi-LEh2H7tMRLzUuyPgMgtzuAFaO4IhGeUDGMhfCrbx1CPWoyZmYyHZjhFxmLFCturCPUBQYKQWgymPI5K5TTHheKzQZXXb8yuH31LJv0n7gfX4',
            'notification' => [
                'title' => 'Сообщение АДС АСУС МКД',
                'body' => '123123',
            ],
        ];


        $fields = json_encode($request_body);

        $request_headers = [
            'Content-Type: application/json',
            'Authorization: key=AAAA8Lw6CEM:APA91bEmtNxBYC87uATp4yd94tFQhKb1GfoM0EMjaTiaQbtKjG1-gCp-22XDy4Lr1LSfWUCsXNlb_htwhx7nP2KvuJqfHQDNKKSASJSPWMezjg3xSCU8vx33N439z05QQy36WXzUiXev',
        ];



        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $response = curl_exec($ch);
        curl_close($ch);

//        \Yii::warning($response, 'Push send response');

//        var_dump($response);
//        exit;

        return $response;
    }

    /**
     * @return array
     */
    public function actionUploadAvatar()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = new AvatarUploadForm();
        $data = ['model' => $request->post()];

        if ($model->load($data, 'model') && $model->upload()) {
            return ['success' => true];
        } else {
            return ['errors' => $this->handleModelErrors($model->errors)];
        }
    }

    /**
     * @return array
     */
    public function actionPermissions()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        return [
            ['id' => Users::USER_ROLE_SUPER_ADMIN, 'name' => 'Супер Администратор',],
            ['id' => Users::USER_ROLE_SUPER_MANAGER, 'name' => 'Супер менеджер',],
            ['id' => Users::USER_ROLE_ADMIN, 'name' => 'Администратор',],
            ['id' => Users::USER_ROLE_MANAGER, 'name' => 'Диспетчер',],
            ['id' => Users::USER_ROLE_SPECIALIST, 'name' => 'Специалист',],
        ];
    }

    /**
     * POST
     */
    public function actionGetToken()
    {
        $request = Yii::$app->request;
        $data = ['model' => $request->post()];
        $model = new LoginForm();

        if($model->load($data, 'model')) {

            $token = $model->login();

            if($token == null){
                return ['errors' => $this->handleModelErrors($model->errors)];
            }

            return ['result' => $token];
        }

        return ['result' => false];
    }

    /**
     * @return null|\yii\web\IdentityInterface
     */
    public function actionMe()
    {
        $user = Yii::$app->user->identity;

        $user->permission = ArrayHelper::getValue([
            Users::USER_ROLE_SUPER_ADMIN => 'Супер Администратор',
            Users::USER_ROLE_SUPER_MANAGER => 'Супер менеджер',
            Users::USER_ROLE_ADMIN => 'Администратор',
            Users::USER_ROLE_MANAGER => 'Менеджер',
            Users::USER_ROLE_SPECIALIST => 'Специалист',
        ], $user->permission);

        return $user;
    }
}