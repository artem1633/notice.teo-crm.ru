<?php
/**
 * Created by PhpStorm.
 * User: Ilusha
 * Date: 19.06.2020
 * Time: 21:06
 */

namespace app\modules\api\controllers;

use Yii;
use yii\base\Controller;
use app\models\User;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

class BaseController extends Controller
{
    /**
     * @param array $modelErrors
     * @return array
     */
    protected function handleModelErrors($modelErrors)
    {
        $errors = [];
        foreach ($modelErrors as $attr => $fldErrors)
        {
            foreach ($fldErrors as $err){
                $errors[] = $err;
            }
        }

        return $errors;
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws BadRequestHttpException
     * @throws ForbiddenHttpException
     */
    public function beforeAction($action)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if($action->id != 'get-token'){
            $request = Yii::$app->request;
            $user = null;
            if($request->isPost){
                if(isset($_POST['token'])){
                    $user = User::find()->where(['token' => $_POST['token']])->one();
                } else {
                    throw new BadRequestHttpException('Authorization token is required');
                }
            } else if($request->isGet){
                if(isset($_GET['token'])){
                    $user = User::find()->where(['token' => $_GET['token']])->one();
                } else {
                    throw new BadRequestHttpException('Authorization token is required');
                }
            }

            if($user == null){
                throw new ForbiddenHttpException('Authorization failed. Token is invalid');
            }

            Yii::$app->user->login($user, 3600 * 24 * 30);
        }

        return parent::beforeAction($action);
    }
}