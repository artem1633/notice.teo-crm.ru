<?php

namespace app\modules\api\controllers;

use app\models\Users;
use Yii;
use app\models\TaskAttachment;
use app\modules\api\models\TaskChangeStatusForm;
use app\modules\api\models\TaskComment;
use app\modules\api\models\TaskFile;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use app\models\Task;
use app\models\TaskSearch;

/**
 * Class TaskController
 * @package app\modules\api\controllers
 */
class TaskController extends BaseController
{
    /**
     * @return array
     */
    public function actionIndex()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $searchModel = new TaskSearch();
        $dataProvider = $searchModel->search(['model' => Yii::$app->request->queryParams], true, true);

        return $dataProvider->models;
    }

    /**
     * @return array
     */
    public function actionProperties()
    {
        $data = [];
        $id = $_GET['id'];

        $children = Task::find()->where(['parent_task_id' => $id])->all();
        $images = TaskAttachment::find()->where(['task_id' => $id, 'type' => TaskAttachment::TYPE_IMAGE])->asArray()->all();
        $sounds = TaskAttachment::find()->where(['task_id' => $id, 'type' => TaskAttachment::TYPE_SOUND])->asArray()->all();
        $comments = TaskAttachment::find()->where(['task_id' => $id, 'type' => TaskAttachment::TYPE_COMMENT])->asArray()->all();
        $history = TaskAttachment::find()->where(['task_id' => $id, 'type' => TaskAttachment::TYPE_HISTORY])->asArray()->all();

        for ($i = 0; $i < count($images); $i++){
            $attach = $images[$i];
            if($attach['user_id'] != null){
                $user = Users::findOne($attach['user_id']);
                if($user){
                    $images[$i]['user'] = $user;
                }
            }
        }
        for ($i = 0; $i < count($sounds); $i++){
            $attach = $sounds[$i];
            if($attach['user_id'] != null){
                $user = Users::findOne($attach['user_id']);
                if($user){
                    $sounds[$i]['user'] = $user;
                }
            }
        }
        for ($i = 0; $i < count($comments); $i++){
            $attach = $comments[$i];
            if($attach['user_id'] != null){
                $user = Users::findOne($attach['user_id']);
                if($user){
                    $comments[$i]['user'] = $user;
                }
            }
        }
        for ($i = 0; $i < count($history); $i++){
            $attach = $history[$i];
            if($attach['user_id'] != null){
                $user = Users::findOne($attach['user_id']);
                if($user){
                    $history[$i]['user'] = $user;
                }
            }
        }

        $data['children'] = $children;
        $data['images'] = $images;
        $data['sounds'] = $sounds;
        $data['comments'] = $comments;
        $data['history'] = $history;

        return $data;
    }

    /**
     * @return array
     */
    public function actionFree()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $searchModel = new TaskSearch();
        $dataProvider = $searchModel->search(['model' => ArrayHelper::merge(Yii::$app->request->queryParams, ['is_fee' => 0])], true, true);

        return $dataProvider->models;
    }

    /**
     * @return array
     */
    public function actionNotFree()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $searchModel = new TaskSearch();
        $dataProvider = $searchModel->search(['model' => ArrayHelper::merge(Yii::$app->request->queryParams, ['is_fee' => 1])], true, true);

        return $dataProvider->models;
    }

    /**
     * @return array
     */
    public function actionCreate()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = new Task();
        $data = ['model' => $request->post()];

        if ($model->load($data, 'model') && $model->save()) {
            return ['success' => true];
        } else {
            Yii::warning($model->errors, 'validation error');
            return ['errors' => $this->handleModelErrors($model->errors)];
        }
    }

    /**
     * Updates an existing Street model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $data = ['model' => $request->post()];
        $model = $this->findModel($data['model']['id']);

        if ($model->load($data, 'model') && $model->save()) {
            return ['success' => true];
        } else {
            \Yii::warning($model->errors, 'Errors Task');
            return ['errors' => $this->handleModelErrors($model->errors)];
        }
    }

    /**
     * Updates an existing Street model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdateStatus()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;

        if(isset($_POST['id']) == null || isset($_POST['status_id']) == null){
            throw new BadRequestHttpException();
        }

        $id = $_POST['id'];
        $status_id = $_POST['status_id'];

        $model = $this->findModel($id);
        $model->status_id = $status_id;

        if ($model->save(false)) {
            return ['success' => true];
        } else {
            \Yii::warning($model->errors, 'Errors Task');
            return ['errors' => $this->handleModelErrors($model->errors)];
        }
    }

    /**
     * @param int $id
     * @return Task
     */
    public function actionView($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = $this->findModel($id);

        return $model;
    }

    /**
     * Delete an existing Street model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if(isset($_POST['id']) == false){
            throw new BadRequestHttpException('Param id is required');
        }
        $this->findModel($_POST['id'])->delete();

        return ['success' => true];
    }

    /**
     * @return array
     */
    public function actionChangeStatus()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = new TaskChangeStatusForm();
        $data = ['model' => $request->post()];

        if ($model->load($data, 'model') && $model->save()) {
            return ['success' => true];
        } else {
            return ['errors' => $this->handleModelErrors($model->errors)];
        }
    }

    /**
     * @return array
     */
    public function actionAddComment()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = new TaskComment();
        $data = ['model' => $request->post()];

        if ($model->load($data, 'model') && $model->save()) {
            return ['success' => true];
        } else {
            return ['errors' => $this->handleModelErrors($model->errors)];
        }
    }

    /**
     * @return array
     */
    public function actionAddImage()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = new TaskFile(['type' => TaskAttachment::TYPE_IMAGE]);
        $data = ['model' => $request->post()];

        if ($model->load($data, 'model') && $model->save()) {
            return ['success' => true];
        } else {
            return ['errors' => $this->handleModelErrors($model->errors)];
        }
    }

    /**
     * @return array
     */
    public function actionAddSound()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = new TaskFile(['type' => TaskAttachment::TYPE_SOUND]);
        $data = ['model' => $request->post()];

        if ($model->load($data, 'model') && $model->save()) {
            return ['success' => true];
        } else {
            return ['errors' => $this->handleModelErrors($model->errors)];
        }
    }

    /**
     * Finds the Street model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Task the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Task::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемая страница не найдена.');
        }
    }
}