<?php

namespace app\modules\api\controllers;

use Yii;
use app\models\Apartment;
use app\models\House;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use app\models\Meter;
use app\models\MeterSearch;
use yii\helpers\ArrayHelper;

/**
 * Class MeterController
 * @package app\modules\api\controllers
 */
class MeterController extends BaseController
{
    /**
     * @return array
     */
    public function actionIndex()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $searchModel = new MeterSearch();
//        $housesPks = ArrayHelper::getColumn(House::find()->where(['company_id' => Yii::$app->user->getId()])->all(), 'id');
//        $apartmentPks = ArrayHelper::getColumn(Apartment::find()->where(['house_id' => $housesPks])->all(), 'house_id');
        $dataProvider = $searchModel->search(['model' => Yii::$app->request->queryParams], true, true);
        $dataProvider->pagination = false;

        return $dataProvider->models;
    }

    /**
     * @return array
     */
    public function actionCreate()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = new Meter();
        $data = ['model' => $request->post()];

        if ($model->load($data, 'model') && $model->save()) {
            return ['success' => true];
        } else {
            return ['errors' => $this->handleModelErrors($model->errors)];
        }
    }

    /**
     * Updates an existing Street model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $data = ['model' => $request->post()];
        $model = $this->findModel($data['model']['id']);

        if ($model->load($data, 'model') && $model->save()) {
            return ['success' => true];
        } else {
            return ['errors' => $this->handleModelErrors($model->errors)];
        }
    }

    /**
     * @return Meter
     */
    public function actionView()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = $this->findModel($_GET['id']);

        return $model;
    }

    /**
     * Delete an existing Street model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if(isset($_POST['id']) == false){
            throw new BadRequestHttpException('Param id is required');
        }
        $this->findModel($_POST['id'])->delete();

        return ['success' => true];
    }

    /**
     * Finds the Street model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Meter the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Meter::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемая страница не найдена.');
        }
    }
}