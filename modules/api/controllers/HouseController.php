<?php

namespace app\modules\api\controllers;

use app\models\Apartment;
use app\models\CrashDisable;
use app\models\Meter;
use app\models\Task;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use app\models\House;
use app\models\HouseSearch;
use yii\web\NotFoundHttpException;

/**
 * Class HouseController
 * @package app\modules\api\controllers
 */
class HouseController extends BaseController
{
    /**
     * @return array
     */
    public function actionIndex()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $searchModel = new HouseSearch();
        $dataProvider = $searchModel->search(['model' => Yii::$app->request->queryParams], true);

        return $dataProvider->models;
    }

    /**
     * @return array
     */
    public function actionProperties()
    {
        $data = [];

        $id = $_GET['id'];

        $tasks = Task::find()->where(['house_id' => $id])->all();
        $crashDisables = CrashDisable::find()->where(['house_id' => $id])->all();
        $apartments = Apartment::find()->where(['house_id' => $id])->all();
        $meters = Meter::find()->where(['apartment_id' => ArrayHelper::getColumn($apartments, 'id')])->all();

        $data['tasks'] = $tasks;
        $data['crashDisables'] = $crashDisables;
        $data['apartments'] = $apartments;
        $data['meters'] = $meters;

        return $data;
    }

    /**
     * @param int $id
     * @return House
     */
    public function actionView($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = $this->findModel($id);

        return $model;
    }

    /**
     * Finds the Street model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return House the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = House::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемая страница не найдена.');
        }
    }

}