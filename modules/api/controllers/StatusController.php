<?php

namespace app\modules\api\controllers;

use app\models\Status;
use Yii;
use yii\web\Response;

/**
 * Class StatusController
 * @package app\modules\api\controllers
 */
class StatusController extends BaseController
{
    /**
     * @return array
     */
    public function actionIndex()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $models = Status::find()->all();

        return $models;
    }
}