<?php

namespace app\modules\api\controllers;

use Yii;
use yii\web\Response;
use app\models\CrashDisable;
use app\models\CrashDisableSearch;
use yii\web\NotFoundHttpException;

/**
 * Class CrashDisableController
 * @package app\modules\api\controllers
 */
class CrashDisableController extends BaseController
{
    /**
     * @return array
     */
    public function actionIndex()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $searchModel = new CrashDisableSearch();
        $dataProvider = $searchModel->search(['model' => Yii::$app->request->queryParams], true, true);
//        $dataProvider->query->andFilterWhere(['company_id' => Yii::$app->user->identity->company_id]);

        return $dataProvider->models;
    }

    /**
     * Updates an existing Street model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $data = ['model' => $request->post()];
        $model = $this->findModel($data['model']['id']);

        if ($model->load($data, 'model') && $model->save()) {
            return ['success' => true];
        } else {
            return ['errors' => $this->handleModelErrors($model->errors)];
        }
    }

    /**
     * @param int $id
     * @return CrashDisable
     */
    public function actionView($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = $this->findModel($id);

        return $model;
    }

    /**
     * Finds the Street model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CrashDisable the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CrashDisable::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемая страница не найдена.');
        }
    }

}