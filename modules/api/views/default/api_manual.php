<?php
$this->title = "Описание АПИ";
?>
<div class="content">
    <div style="display: none;">
        <p>Методы:</p>
        <p>get-specialist - Возвращает профиль мастера</p>
        <p>get-petition-statuses - Возвращает статусы заявки</p>
        <p>get-petitions - Возвращает заявки</p>
        <p>get-petition - Возвращает заявку</p>
        <p>accept-petition - Принятие заявки специалистом</p>
        <p>not-accept-petition - Отклонение заявки специалистом</p>
        <p>get-resident - Возвращает информации по жильцу</p>
        <p>----------------------------------</p>
        <p>get-specialist</p>
    </div>

    <div>
        <p style="color: red;">ВО ВСЕХ ЗАПРОСАХ (КРОМЕ МЕТОДА get-token) ДОЛЖЕН ПЕРЕДОВАТЬСЯ ПАРАМЕТР token ДЛЯ АВТОРИЗАЦИИ ПОЛЬЗОВАТЕЛЯ. ПАРАМЕТР ПЕРЕДАЕТСЯ ЛИБО В GET ЛИБО POST ЗАПРОСЕ В ЗАВИСИМОСТИ ОТ МЕТОДА</p>

        <h4>Авторизация</h4>
        <b>/api/user/get-token [POST]</b>
        <p>username String *</p>
        <p>password String *</p>
        <b>Возвращает: {"result": TOKEN}</b>
        <hr>

        <h4>Установка FCM токена</h4>
        <b>/api/user/set-fcm-token [POST]</b>
        <p>fcm_token String *</p>
        <b>Возвращает: {"result": true}</b>
        <hr>

        <h4>Загрузить аватар</h4>
        <b>/api/user/upload-avatar [POST]</b>
        <p>file FILE *</p>
        <b>Возвращает: {"result": true}</b>
        <hr>

        <h4>Информация об аккаунте</h4>
        <b>/api/user/me [GET]</b>
        <b>Возвращает информацию об аккаунте</b>
        <hr>

        <h4>Получить должности</h4>
        <b>/api/user/permissions [GET]</b>
        <b>Возвращает должности</b>
        <hr>

        <h4>Получить список всех исполнителей</h4>
        <b>api/user/executors</b>
        <b>Возвращает список</b>
        <hr>

        <h4>Получение списка бесплатных задач</h4>
        <b>/api/task/free [GET]</b>
        <p>status_id int Статус (Внешняя связь со Status) в случае поиска</p>
        <p>address String Адрес задачи в случае поиска</p>
        <b>Возвращает список задач</b>
        <hr>

        <h4>Получение списка платных задач</h4>
        <b>/api/task/not-free [GET]</b>
        <p>status_id int Статус (Внешняя связь со Status) в случае поиска</p>
        <p>address String Адрес задачи в случае поиска</p>
        <b>Возвращает список задач</b>
        <hr>

        <h4>Просмотреть информацию о задачи</h4>
        <b>/api/task/properties [GET]</b>
        <p>id int * ID задачи</p>
        <b>Возращает {
                "children": [...],
                "images": [...],
                "sounds": [...],
                "comments": [...],
                "history": [...]
            }</b>
        <hr>

        <h4>Создать задачу</h4>
        <b>/api/task/create [POST]</b>
        <p>petition_id int Обращение</p>
        <p>house_id int Дом</p>
        <p>executor_id int Исполнитель</p>
        <p>status_id int (Внешняя связь с Status) Статус</p>
        <p>company_id int Компания</p>
        <p>is_fee int Платно</p>
        <p>execute_datetime String (Формат: YYYY-MM-DD HH:II:SS) Срок исполнения</p>
        <p>description String Описание</p>
        <p>description_short STRING Краткое описание</p>
        <p>priority int Приоритет</p>
        <p>child_task_id int Подзадача</p>
        <b>Возращает результат создания {"result": true}</b>
        <hr>

        <h4>Изменить зачачу</h4>
        <b>/api/task/update [POST]</b>
        <p>id int * ID задачи</p>
        <p>petition_id int Обращение</p>
        <p>house_id int Дом</p>
        <p>executor_id int Исполнитель</p>
        <p>status_id int (Внешняя связь с Status) Статус</p>
        <p>company_id int Компания</p>
        <p>is_fee int Платно</p>
        <p>execute_datetime String (Формат: YYYY-MM-DD HH:II:SS) Срок исполнения</p>
        <p>description String Описание</p>
        <p>description_short STRING Краткое описание</p>
        <p>priority int Приоритет</p>
        <p>child_task_id int Подзадача</p>
        <b>Возращает результат изменения {"result": true}</b>
        <hr>

        <h4>Просмотреть зачачу</h4>
        <b>/api/task/view [GET]</b>
        <p>id int * ID задачи</p>
        <b>Возращает задачу</b>
        <hr>

        <h4>Удалить зачачу</h4>
        <b>/api/task/delete [POST]</b>
        <p>id int * ID задачи</p>
        <b>Возращает результат удаления</b>
        <hr>

        <h4>Получение списка статусов</h4>
        <b>/api/status/index [GET]</b>
        <b>Возвращает список задач</b>
        <hr>

        <h4>Изменить статус зачачи</h4>
        <b>/api/task/change-status [POST]</b>
        <p>taskId int (Внешняя связь с Status) * ID задачи</p>
        <p>status int * ID статуса</p>
        <p>coords String * Координаты разделенные ";"</p>
        <b>Возращает задачу</b>
        <hr>

        <h4>Добавить комментарий к задаче</h4>
        <b>/api/task/add-comment [POST]</b>
        <p>taskId int * ID задачи</p>
        <p>text String * Текст</p>
        <b>Возращает результат</b>
        <hr>

        <h4>Добавить изображение к задаче</h4>
        <b>/api/task/add-image [POST]</b>
        <p>taskId int * ID задачи</p>
        <p>file file * Файл</p>
        <b>Возращает результат</b>
        <hr>

        <h4>Добавить аудио к задаче</h4>
        <b>/api/task/add-sound [POST]</b>
        <p>taskId int * ID задачи</p>
        <p>file file * Файл</p>
        <b>Возращает результат</b>
        <hr>

        <h4>Получить все заявки</h4>
        <b>/api/petition/index [GET]</b>
        <b>Возращает список заявок</b>
        <hr>

        <h4>Создать заявку</h4>
        <b>/api/petition/create [POST]</b>
        <p>header String Тема обращения</p>
        <p>text String Содержание обращения</p>
        <p>status_id int ID статуса обращения</p>
        <p>specialist_id int ID Специалиста</p>
        <p>manager_id int ID Менеджера</p>
        <p>resident_id int Заявитель</p>
        <p>relation_petition_id int ID связанноого обращения</p>
        <p>execution_date String (В формате YYYY-MM-DD)Дата исполнения обращения</p>
        <p>created_by int ID создавшего обращение</p>
        <p>answer String Ответ заявителю</p>
        <p>closed_user_id int Пользователь, закрывший обращение</p>
        <p>petition_type int Тип обращения</p>
        <p>address String Адрес</p>
        <p>house int Дом</p>
        <p>apartment int Квартира</p>
        <p>trouble_id int ID Неисправности</p>
        <p>trouble_description String Описание Неисправности</p>
        <p>call_id int ID звонка</p>
        <b>Возращает результат создания {"result": true}</b>
        <hr>

        <h4>Изменить заявку</h4>
        <b>/api/petition/update[POST]</b>
        <p>id int * ID заявки</p>
        <p>header String Тема обращения</p>
        <p>text String Содержание обращения</p>
        <p>status_id int ID статуса обращения</p>
        <p>specialist_id int ID Специалиста</p>
        <p>manager_id int ID Менеджера</p>
        <p>resident_id int Заявитель</p>
        <p>relation_petition_id int ID связанноого обращения</p>
        <p>execution_date String (В формате YYYY-MM-DD)Дата исполнения обращения</p>
        <p>created_by int ID создавшего обращение</p>
        <p>answer String Ответ заявителю</p>
        <p>closed_user_id int Пользователь, закрывший обращение</p>
        <p>petition_type int Тип обращения</p>
        <p>address String Адрес</p>
        <p>house int Дом</p>
        <p>apartment int Квартира</p>
        <p>trouble_id int ID Неисправности</p>
        <p>trouble_description String Описание Неисправности</p>
        <p>call_id int ID звонка</p>
        <b>Возращает результат создания {"result": true}</b>
        <hr>

        <h4>Просмотреть заявку</h4>
        <b>/api/petition/view [GET]</b>
        <p>id int * ID заявки</p>
        <b>Возращает задачу</b>
        <hr>

        <h4>Удалить заявку</h4>
        <b>/api/petition/delete [POST]</b>
        <p>id int * ID заявки</p>
        <b>Возращает результат удаления</b>
        <hr>

        <h4>Получить все показания</h4>
        <b>/api/meter-info/index [GET]</b>
        <b>Возращает список показаний</b>
        <hr>

        <h4>Передать показания счетчика</h4>
        <b>/api/meter-info/create [POST]</b>
        <p>meter_id int Прибор учета</p>
        <p>created_at String (Формат дата: YYYY-MM-DD HH:II:SS) Дата сведений</p>
        <p>updated_at String (Формат дата: YYYY-MM-DD HH:II:SS) Дата изменения сведений</p>
        <p>value Double Показания</p>
        <p>apartment_id int Помещение</p>
        <p>file file Фото</p>
        <b>Возращает результат создания {"result": true}</b>
        <hr>

        <h4>Изменить показания счетчика</h4>
        <b>/api/meter-info/update [POST]</b>
        <p>id int ID показания</p>
        <p>meter_id int Прибор учета</p>
        <p>created_at String (Формат дата: YYYY-MM-DD HH:II:SS) Дата сведений</p>
        <p>updated_at String (Формат дата: YYYY-MM-DD HH:II:SS) Дата изменения сведений</p>
        <p>value Double Показания</p>
        <p>apartment_id int Помещение</p>
        <p>file file Фото</p>
        <b>Возращает результат изменения {"result": true}</b>
        <hr>

        <h4>Просмотреть показания</h4>
        <b>/api/meter-info/view [GET]</b>
        <p>id int * ID показания</p>
        <b>Возращает задачу</b>
        <hr>

        <h4>Удалить заявку</h4>
        <b>/api/meter-info/delete [POST]</b>
        <p>id int * ID показания</p>
        <b>Возращает результат удаления</b>
        <hr>

        <h4>Получить все приборы учета</h4>
        <b>/api/meter/index [GET]</b>
        <p>address String Поиск по адресу</p>
        <b>Возращает список Приборов</b>
        <hr>

        <h4>Создать прибор учета</h4>
        <b>/api/meter/create [POST]</b>
        <p>apartment_id int Помещение</p>
        <p>number int Номер прибора</p>
        <p>type int (1 = Горячая вода; 2 = Холодная вода; 3 = Газ; 4 = Отопление; 5 = Электричество) Что учитывает прибор</p>
        <p>note String Примечание</p>
        <p>date_verify String (Формат: YYYY-MM-DD HH:II:SS) Дата проверки</p>
        <b>Возращает результат создания {"result": true}</b>
        <hr>

        <h4>Изменить прибор учета</h4>
        <b>/api/meter/update [POST]</b>
        <p>id int ID прибора учета</p>
        <p>apartment_id int Помещение</p>
        <p>number int Номер прибора</p>
        <p>type int (1 = Горячая вода; 2 = Холодная вода; 3 = Газ; 4 = Отопление; 5 = Электричество) Что учитывает прибор</p>
        <p>note String Примечание</p>
        <p>date_verify String (Формат: YYYY-MM-DD HH:II:SS) Дата проверки</p>
        <b>Возращает результат изменения {"result": true}</b>
        <hr>

        <h4>Просмотреть прибор учета</h4>
        <b>/api/meter/view [GET]</b>
        <p>id int * ID прибора</p>
        <b>Возращает прибор</b>
        <hr>

        <h4>Удалить прибор учета</h4>
        <b>/api/meter/delete [POST]</b>
        <p>id int * ID прибора</p>
        <b>Возращает результат удаления</b>
        <hr>

        <h4>Получить все дома</h4>
        <b>/api/house/index [GET]</b>
        <b>Возращает список домов</b>
        <hr>

        <h4>Просмотреть информацию о здании</h4>
        <b>/api/house/properties [GET]</b>
        <p>id int * ID ав. отключения</p>
        <b>Возращает {
                "tasks": [...],
                "crashDisables": [...],
                "apartments": [...],
                "meters": [...]
            }</b>
        <hr>

        <h4>Получить всех аварийных отключений</h4>
        <b>/api/crash-disable/index [GET]</b>
        <b>Возращает список аварийных отключений</b>
        <hr>

        <h4>Создать аварийное отключение</h4>
        <b>/api/crash-disable/create [POST]</b>
        <p>house_id int Дом</p>
        <p>start_datetime String (Формат YYYY-MM-DD HH:II:SS) Дата и время начала отключения</p>
        <p>plan_end_datetime String (Формат YYYY-MM-DD HH:II:SS) Планируемая дата и время окончания</p>
        <p>fact_end_datetime String (Формат YYYY-MM-DD HH:II:SS) Фактическая дата и время окончания</p>
        <p>resource int (0 = Газ; 1 = Холодная вода; 2 = Электричество; 3 = Отопление; 5 = Горячая вода) Ресурс</p>
        <p>status int (0 = Новый; 1 = В работе; 2 = Ликвидировано) Статус</p>
        <b>Возращает результат создания {"result": true}</b>
        <hr>

        <h4>Изменить аварийное отключение</h4>
        <b>/api/crash-disable/update [POST]</b>
        <p>id int ID аварийного отключения</p>
        <p>house_id int Дом</p>
        <p>start_datetime String (Формат YYYY-MM-DD HH:II:SS) Дата и время начала отключения</p>
        <p>plan_end_datetime String (Формат YYYY-MM-DD HH:II:SS) Планируемая дата и время окончания</p>
        <p>fact_end_datetime String (Формат YYYY-MM-DD HH:II:SS) Фактическая дата и время окончания</p>
        <p>resource int (0 = Газ; 1 = Холодная вода; 2 = Электричество; 3 = Отопление; 5 = Горячая вода) Ресурс</p>
        <p>status int (0 = Новый; 1 = В работе; 2 = Ликвидировано) Статус</p>
        <b>Возращает результат изменения {"result": true}</b>
        <hr>

        <h4>Просмотреть ав. отключение</h4>
        <b>/api/crash-disable/view [GET]</b>
        <p>id int * ID ав. отключения</p>
        <b>Возращает прибор</b>
        <hr>

        <h4>Удалить ав. отключение</h4>
        <b>/api/crash-disable/delete [POST]</b>
        <p>id int * ID ав. отключения</p>
        <b>Возращает результат удаления</b>
        <hr>

    </div>
</div>
