<?php

namespace app\modules\api\models;

use app\models\TaskAttachment;
use yii\base\Model;

/**
 * Class TaskHistory
 * @package app\modules\api\models
 */
class TaskHistory extends Model
{
    /**
     * @var int
     */
    public $taskId;

    /**
     * @var string
     */
    public $text;

    /**
     * @var string
     */
    public $coords;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['taskId', 'text', 'coords'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'taskId' => 'Задача',
            'text' => 'Текст',
            'coords' => 'Координаты',
        ];
    }

    /**
     * @return bool
     */
    public function save()
    {
        if($this->validate()){
            return (new TaskAttachment([
                'task_id' => $this->taskId,
                'param1' => $this->text,
                'param2' => $this->coords,
                'type' => TaskAttachment::TYPE_COMMENT,
            ]))->save(false);
        }

        return false;
    }
}