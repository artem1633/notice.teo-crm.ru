<?php

namespace app\modules\api\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class AvatarUploadForm
 * @package app\modules\api\models
 */
class AvatarUploadForm extends Model
{
    /** @var UploadedFile */
    public $file;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['file', 'file'],
        ];
    }

    /**
     * @return boolean
     */
    public function upload()
    {
        if($this->validate()){
            $this->file = UploadedFile::getInstanceByName('file');

            if(is_dir('uploads') == false){
                mkdir('uploads');
            }

            $name = Yii::$app->security->generateRandomString();
            $path = "uploads/{$name}.{$this->file->extension}";

            $this->file->saveAs($path);

            Yii::$app->user->identity->avatar = $path;
            Yii::$app->user->identity->save(false);
        }

        return false;
    }
}