<?php

namespace app\modules\api\models;

use app\models\TaskAttachment;
use yii\base\Model;

/**
 * Class TaskComment
 * @package app\modules\api\models
 */
class TaskComment extends Model
{
    /**
     * @var int
     */
    public $taskId;

    /**
     * @var string
     */
    public $text;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['taskId', 'text'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'taskId' => 'Задача',
            'text' => 'Текст',
        ];
    }

    /**
     * @return bool
     */
    public function save()
    {
        if($this->validate()){
            return (new TaskAttachment([
                'task_id' => $this->taskId,
                'param1' => $this->text,
                'type' => TaskAttachment::TYPE_COMMENT,
            ]))->save(false);
        }

        return false;
    }
}