<?php

namespace app\modules\api\models;

use app\models\Task;
use app\models\TaskAttachment;
use yii\base\Model;

/**
 * Class TaskChangeStatusForm
 * @package app\modules\api\models
 */
class TaskChangeStatusForm extends Model
{
    /**
     * @var int
     */
    public $taskId;

    /**
     * @var int
     */
    public $status;

    /**
     * @var string
     */
    public $coords;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['taskId', 'status', 'coords'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'taskId' => 'Задача',
            'status' => 'Статус',
            'coords' => 'Координаты',
        ];
    }

    /**
     * @return bool
     */
    public function save()
    {
        if($this->validate()){
            $task = Task::findOne($this->taskId);

            if($task){
                $oldStatus = $task->status_id;
                $task->status_id = $this->status;
                $task->save(false);

                $attachment = new TaskAttachment([
                    'task_id' => $this->taskId,
                    'param1' => "Статус изменен с «{$oldStatus}» на «{$this->status}»",
                    'param2' => $this->coords,
                    'type' => TaskAttachment::TYPE_HISTORY
                ]);

                return $attachment->save(false);
            }
        }

        return false;
    }
}