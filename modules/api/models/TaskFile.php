<?php

namespace app\modules\api\models;

use Yii;
use app\models\TaskAttachment;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class TaskFile
 * @package app\modules\api\models
 */
class TaskFile extends Model
{
    /**
     * @var int
     */
    public $taskId;

    /**
     * @var UploadedFile
     */
    public $file;

    /**
     * @var int
     */
    public $type;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['taskId', 'type'], 'required'],
            ['file', 'file'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'taskId' => 'Задача',
            'file' => 'Файл',
            'type' => 'Тип',
        ];
    }

    /**
     * @return bool
     */
    public function save()
    {
        if($this->validate()){

            $this->file = UploadedFile::getInstanceByName('file');

            if(is_dir('uploads') == false){
                mkdir('uploads');
            }

            $name = Yii::$app->security->generateRandomString();
            $path = "uploads/{$name}.{$this->file->extension}";

            $this->file->saveAs($path);

            return (new TaskAttachment([
                'task_id' => $this->taskId,
                'param1' => $path,
                'type' => $this->type,
            ]))->save(false);
        }

        return false;
    }
}