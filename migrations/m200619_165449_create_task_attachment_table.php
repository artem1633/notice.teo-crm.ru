<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%task_attachment}}`.
 */
class m200619_165449_create_task_attachment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%task_attachment}}', [
            'id' => $this->primaryKey(),
            'task_id' => $this->integer()->comment('Задача'),
            'param1' => $this->string()->comment('Параметр'),
            'param2' => $this->string()->comment('Параметр 2'),
            'type' => $this->integer()->comment('Тип'),
            'user_id' => $this->integer()->comment('Пользователь'),
            'created_at' => $this->dateTime()->comment('Дата и время добавления'),
        ]);

        $this->createIndex(
            'idx-task_attachment-task_id',
            'task_attachment',
            'task_id'
        );

        $this->addForeignKey(
            'fk-task_attachment-task_id',
            'task_attachment',
            'task_id',
            'task',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-task_attachment-user_id',
            'task_attachment',
            'user_id'
        );

        $this->addForeignKey(
            'fk-task_attachment-user_id',
            'task_attachment',
            'user_id',
            'users',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-task_attachment-task_id',
            'task_attachment'
        );

        $this->dropIndex(
            'idx-task_attachment-task_id',
            'task_attachment'
        );

        $this->dropForeignKey(
            'fk-task_attachment-user_id',
            'task_attachment'
        );

        $this->dropIndex(
            'idx-task_attachment-user_id',
            'task_attachment'
        );

        $this->dropTable('{{%task_attachment}}');
    }
}
