<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `{{%apartment_meter}}`.
 */
class m200526_074701_drop_apartment_meter_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('{{%apartment_meter}}');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200526_074701_drop_apartment_meter_table cannot be reverted.\n";

        return false;
    }
}
