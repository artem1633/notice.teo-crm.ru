<?php

use yii\db\Migration;

/**
 * Handles adding child_task_id to table `{{%task}}`.
 */
class m200620_180448_add_child_task_id_column_to_task_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('task', 'child_task_id', $this->integer()->comment('Подзадача'));

        $this->createIndex(
            'idx-task-child_task_id',
            'task',
            'child_task_id'
        );

        $this->addForeignKey(
            'fk-task-child_task_id',
            'task',
            'child_task_id',
            'task',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-task-child_task_id',
            'task'
        );

        $this->dropIndex(
            'idx-task-child_task_id',
            'task'
        );

        $this->dropColumn('task', 'child_task_id');
    }
}
