<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%status_to_color}}`.
 */
class m200525_081138_create_status_color_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%status_color}}', [
            'id' => $this->primaryKey(),
            'status_id' => $this->integer()->comment('Статус'),
            'color' => $this->string()->comment('Цвет'),
            'company_id' => $this->integer()->comment('Компания')
        ]);

        $this->addForeignKey(
            'fk-status_color-status_id',
            '{{%status_color}}',
            'status_id',
            'status',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-status_color-company_id',
            '{{%status_color}}',
            'company_id',
            'company',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%status_color}}');
    }
}
