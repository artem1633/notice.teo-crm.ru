<?php

use yii\db\Migration;

/**
 * Class m200619_165950_add_new_colums_to_users_table
 */
class m200619_165950_add_new_colums_to_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users', 'token', $this->string()->comment('Токен'));
        $this->addColumn('users', 'fcm_token', $this->string()->comment('FCM Токен'));
        $this->addColumn('users', 'coords', $this->string()->comment('Координаты'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('users', 'token');
        $this->dropColumn('users', 'fcm_token');
        $this->dropColumn('users', 'coords');
    }
}
