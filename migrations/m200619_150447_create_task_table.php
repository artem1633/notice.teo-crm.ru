<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%task}}`.
 */
class m200619_150447_create_task_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%task}}', [
            'id' => $this->primaryKey(),
            'petition_id' => $this->integer()->comment('Обращение'),
            'house_id' => $this->integer()->comment('Дом'),
            'executor_id' => $this->integer()->comment('Исполнитель'),
            'status_id' => $this->integer()->comment('Статус'),
            'execute_datetime' => $this->dateTime()->comment('Срок исполнения'),
            'description' => $this->text()->comment('Описание'),
            'description_short' => $this->text()->comment('Краткое описание'),
            'priority' => $this->integer()->comment('Приоритет'),
            'is_fee' => $this->boolean()->defaultValue(false)->comment('Платно'),
            'company_id' => $this->integer()->comment('Компания'),
            'created_at' => $this->dateTime()->comment('Дата и время создания'),
            'created_by' => $this->integer()->comment('Создатель'),
        ]);

        $this->createIndex(
            'idx-task-status_id',
            'task',
            'status_id'
        );

        $this->addForeignKey(
            'fk-task-status_id',
            'task',
            'status_id',
            'status',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-task-petition_id',
            'task',
            'petition_id'
        );

        $this->addForeignKey(
            'fk-task-petition_id',
            'task',
            'petition_id',
            'petition',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-task-created_by',
            'task',
            'created_by'
        );

        $this->addForeignKey(
            'fk-task-created_by',
            'task',
            'created_by',
            'users',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-task-house_id',
            'task',
            'house_id'
        );

        $this->addForeignKey(
            'fk-task-house_id',
            'task',
            'house_id',
            'house',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-task-executor_id',
            'task',
            'executor_id'
        );

        $this->addForeignKey(
            'fk-task-executor_id',
            'task',
            'executor_id',
            'users',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-task-company_id',
            'task',
            'company_id'
        );

        $this->addForeignKey(
            'fk-task-company_id',
            'task',
            'company_id',
            'company',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-task-status_id',
            'task'
        );

        $this->dropIndex(
            'idx-task-status_id',
            'task'
        );

        $this->dropForeignKey(
            'fk-task-petition_id',
            'task'
        );

        $this->dropIndex(
            'idx-task-petition_id',
            'task'
        );

        $this->dropForeignKey(
            'fk-task-created_by',
            'task'
        );

        $this->dropIndex(
            'idx-task-created_by',
            'task'
        );

        $this->dropForeignKey(
            'fk-task-house_id',
            'task'
        );

        $this->dropIndex(
            'idx-task-house_id',
            'task'
        );

        $this->dropForeignKey(
            'fk-task-executor_id',
            'task'
        );

        $this->dropIndex(
            'idx-task-executor_id',
            'task'
        );

        $this->dropForeignKey(
            'fk-task-company_id',
            'task'
        );

        $this->dropIndex(
            'idx-task-company_id',
            'task'
        );

        $this->dropTable('{{%task}}');
    }
}
