<?php

use yii\db\Migration;

/**
 * Handles adding houses to table `{{%users}}`.
 */
class m200619_132213_add_houses_column_to_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users', 'houses', $this->string()->comment('Дома'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('users', 'houses');
    }
}
