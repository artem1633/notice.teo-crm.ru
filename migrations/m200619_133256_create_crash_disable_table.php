<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%crash_disable}}`.
 */
class m200619_133256_create_crash_disable_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%crash_disable}}', [
            'id' => $this->primaryKey(),
            'house_id' => $this->integer()->comment('Дом'),
            'start_datetime' => $this->dateTime()->comment('Дата и время начала отключения'),
            'plan_end_datetime' => $this->dateTime()->comment('Планируемая дата и время окончания'),
            'fact_end_datetime' => $this->dateTime()->comment('Фактическая дата и время окончания'),
            'resource' => $this->string()->comment('Ресурс'),
            'status' => $this->string()->comment('Статус'),
            'company_id' => $this->integer()->comment('Компания'),
            'created_at' => $this->dateTime()->comment('Дата и время создания'),
            'created_by' => $this->integer()->comment('Создатель'),
        ]);

        $this->createIndex(
            'idx-crash_disable-house_id',
            'crash_disable',
            'house_id'
        );

        $this->addForeignKey(
            'fk-crash_disable-house_id',
            'crash_disable',
            'house_id',
            'house',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-crash_disable-company_id',
            'crash_disable',
            'company_id'
        );

        $this->addForeignKey(
            'fk-crash_disable-company_id',
            'crash_disable',
            'company_id',
            'company',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-crash_disable-created_by',
            'crash_disable',
            'created_by'
        );

        $this->addForeignKey(
            'fk-crash_disable-created_by',
            'crash_disable',
            'created_by',
            'users',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-crash_disable-created_by',
            'crash_disable'
        );

        $this->dropIndex(
            'idx-crash_disable-created_by',
            'crash_disable'
        );

        $this->dropForeignKey(
            'fk-crash_disable-company_id',
            'crash_disable'
        );

        $this->dropIndex(
            'idx-crash_disable-company_id',
            'crash_disable'
        );

        $this->dropForeignKey(
            'fk-crash_disable-house_id',
            'crash_disable'
        );

        $this->dropIndex(
            'idx-crash_disable-house_id',
            'crash_disable'
        );

        $this->dropTable('{{%crash_disable}}');
    }
}
