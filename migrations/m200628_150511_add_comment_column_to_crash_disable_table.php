<?php

use yii\db\Migration;

/**
 * Handles adding comment to table `{{%crash_disable}}`.
 */
class m200628_150511_add_comment_column_to_crash_disable_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('crash_disable', 'comment', $this->text()->comment('Комментарий'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('crash_disable', 'comment');
    }
}
