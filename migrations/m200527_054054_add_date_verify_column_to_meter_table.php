<?php

use yii\db\Migration;

/**
 * Handles adding date_verify to table `{{%meter}}`.
 */
class m200527_054054_add_date_verify_column_to_meter_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%meter}}', 'date_verify', $this->timestamp()
            ->defaultValue(null)->comment('Дата проверки'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%meter}}', 'date_verify');
    }
}
