<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%apartment_meter}}`.
 */
class m200525_115120_create_apartment_meter_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%apartment_meter}}', [
            'id' => $this->primaryKey(),
            'apartment_id' => $this->integer()->comment('Помещение'),
            'cold_water' => $this->double(3)->comment('Холодная вода'),
            'hot_water' => $this->double(3)->comment('Горячая вода'),
            'heat' => $this->double(3)->comment('Отопление'),
            'gas' => $this->double(3)->comment('Газ'),
            'electricity' => $this->double(1)->comment('Электроэнергия'),
            'meter_number' => $this->string()->comment('Номер прибора учета'),
            'next_date_check' => $this->timestamp()->defaultValue(null)->comment('Дата следующей проверки'),
            'created_at' => $this->timestamp()->defaultExpression('NOW()')->comment('Дата предоставления'),
        ]);


        $this->addForeignKey(
            'fk-apartment_meter-apartment_id',
            '{{%apartment_meter}}',
            'apartment_id',
            'apartment',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%apartment_meter}}');
    }
}
