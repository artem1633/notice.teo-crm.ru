<?php

use yii\db\Migration;

/**
 * Handles adding file_path to table `{{%meter_info}}`.
 */
class m200620_132927_add_file_path_column_to_meter_info_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('meter_info', 'file_path', $this->string()->comment('Фото'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('meter_info', 'file_path');
    }
}
