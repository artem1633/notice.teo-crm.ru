<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%meter_info}}`.
 */
class m200526_074907_create_meter_info_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%meter_info}}', [
            'id' => $this->primaryKey(),
            'meter_id' => $this->integer()->comment('Прибор учета'),
            'created_at' => $this->timestamp()->defaultExpression('NOW()')->comment('Дата сведений'),
            'updated_at' => $this->timestamp()->append('ON UPDATE CURRENT_TIMESTAMP')->comment('Дата обновления сведений'),
            'value' => $this->double(3)->comment('Показания (значение прибора учета)')
        ]);

        $this->addForeignKey(
            'fk-meter_info-meter_id',
            '{{%meter_info}}',
            'meter_id',
            '{{%meter}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%meter_info}}');
    }
}
