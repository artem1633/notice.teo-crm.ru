<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%meter}}`.
 */
class m200526_074825_create_meter_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%meter}}', [
            'id' => $this->primaryKey(),
            'apartment_id' => $this->integer()->comment('Помещение'),
            'number' => $this->string()->comment('Номер прибора'),
            'type' => $this->integer()->comment('Тип счетчика (газ, вода и пр.)'),
            'note' => $this->text()->comment('Примечание')
        ]);

        $this->addForeignKey(
            'fk-meter-apartment_id',
            '{{%meter}}',
            'apartment_id',
            '{{%apartment}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%meter}}');
    }
}
