<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%house_meter}}`.
 */
class m200525_115125_create_house_meter_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%house_meter}}', [
            'id' => $this->primaryKey(),
            'house_id' => $this->integer()->comment('Дом'),
            'cold_water' => $this->double(3)->comment('Холодная вода'),
            'hot_water' => $this->double(3)->comment('Горячая вода'),
            'heat' => $this->double(3)->comment('Отопление'),
            'gas' => $this->double(3)->comment('Газ'),
            'electricity' => $this->double(1)->comment('Электроэнергия'),
            'created_at' => $this->timestamp()->defaultExpression('NOW()')->comment('Дата предоставления'),
        ]);

        $this->addForeignKey(
            'fk-house_meter-house_id',
            '{{%house_meter}}',
            'house_id',
            'house',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%house_meter}}');
    }
}
