<?php

use yii\db\Migration;

/**
 * Handles adding parent_task_id to table `{{%task}}`.
 */
class m200626_115508_add_parent_task_id_column_to_task_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('task', 'parent_task_id', $this->integer()->comment('Родительская задача'));

        $this->createIndex(
            'idx-task-parent_task_id',
            'task',
            'parent_task_id'
        );

        $this->addForeignKey(
            'fk-task-parent_task_id',
            'task',
            'parent_task_id',
            'task',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-task-parent_task_id',
            'task'
        );

        $this->dropIndex(
            'idx-task-parent_task_id',
            'task'
        );

        $this->dropColumn('task', 'parent_task_id');
    }
}
