<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%knowledge_base}}`.
 */
class m200526_125359_create_knowledge_base_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%knowledge_base}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Заголовок'),
            'content' => $this->text()->comment('Содержание')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%knowledge_base}}');
    }
}
