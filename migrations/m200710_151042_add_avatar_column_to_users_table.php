<?php

use yii\db\Migration;

/**
 * Handles adding avatar to table `{{%users}}`.
 */
class m200710_151042_add_avatar_column_to_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users', 'avatar', $this->string()->comment('Фото'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('users', 'avatar');
    }
}
