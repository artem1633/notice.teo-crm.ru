<?php

namespace app\components;

use yii\helpers\ArrayHelper;

/**
 * Class FcmPusher
 * @package app\components
 */
class FcmPusher {

    const TYPE_FORM = 'form';
    const TYPE_TICKET = 'ticket';

    /**
     * @param $recipient
     * @param $message
     * @param array $data
     * @return mixed
     */
    public static function push($recipient, $message, $data = [])
    {
        $url = 'https://fcm.googleapis.com/fcm/send';

        $request_body = [
            'to' => 'cJzLQBXHTKWR9wL4aU4VfZ%3AAPA91bFcf-0IbKuLwteFIn5D7qyzSmahzXR2fB82N4MlZ_w5f-ckHjBQ7pg0GBa85G6tmuvU7tWiT1se_fDJqu1s1bRMHLJ7Cp_7OeCFpg58zTaditlNJd0dNO77q3T91By6dhdIlBJw',
            'notification' => [
                'title' => 'Сообщение АДС АСУС МКД',
                'body' => $message,
            ],
        ];

        if(count($data) > 0){
            $request_body = ArrayHelper::merge($request_body, ['data' => $data]);
        }

        $fields = json_encode($request_body);

        $request_headers = [
            'Content-Type: application/json',
            'Authorization: key=AAAA8Lw6CEM:APA91bEmtNxBYC87uATp4yd94tFQhKb1GfoM0EMjaTiaQbtKjG1-gCp-22XDy4Lr1LSfWUCsXNlb_htwhx7nP2KvuJqfHQDNKKSASJSPWMezjg3xSCU8vx33N439z05QQy36WXzUiXev',
        ];



        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $response = curl_exec($ch);
        curl_close($ch);

//        \Yii::warning($response, 'Push send response');

//        var_dump($response);
//        exit;

        return $response;
    }
}