<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Task */
/* @var $form yii\widgets\ActiveForm */

//$petitions = ArrayHelper::map(\app\models\Petition::find()->andWhere(['company.id' => $company_id])->all(), 'id', 'header');
$houses = ArrayHelper::map(\app\models\House::find()->where(['company_id' => Yii::$app->user->identity->company_id])->all(), 'id', 'address')

?>

<div class="task-form">

    <?php $form = ActiveForm::begin(); ?>


    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'petition_id')->widget(Select2::class, [
                'data' => ArrayHelper::map(\app\models\Petition::find()->all(), 'id', 'header'),
                'pluginOptions' => [
                    'allowClear' => true,
                    'placeholder' => 'Выберите',
                ],
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'house_id')->widget(Select2::class, [
                'data' => ArrayHelper::map(\app\models\House::find()->where(['company_id' => Yii::$app->user->identity->company_id])->all(), 'id', 'address'),
                'pluginOptions' => [
                    'allowClear' => true,
                    'placeholder' => 'Выберите',
                ],
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'executor_id')->widget(Select2::class, [
                'data' => ArrayHelper::map(\app\models\Users::find()->where(['company_id' => Yii::$app->user->identity->company_id])->all(), 'id', 'fio'),
                'pluginOptions' => [
                    'allowClear' => true,
                    'placeholder' => 'Выберите',
                ],
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'status_id')->widget(Select2::class, [
                'data' => ArrayHelper::map(\app\models\Status::find()->all(), 'id', 'name'),
                'pluginOptions' => [
                    'allowClear' => true,
                    'placeholder' => 'Выберите',
                ],
            ]) ?>
        </div>
    </div>

    <?= $form->field($model, 'execute_datetime')->widget(\kartik\datetime\DateTimePicker::class, [

    ]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'description_short')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'parent_task_id')->widget(Select2::class, [
        'data' => ArrayHelper::map(\app\models\Task::find()->where(['company_id' => Yii::$app->user->identity->company_id])->all(), 'id', 'description_short'),
        'pluginOptions' => [
            'allowClear' => true,
            'placeholder' => 'Выберите',
        ],
    ]) ?>


    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'priority')->checkbox() ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'is_fee')->checkbox() ?>
        </div>
    </div>

	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
