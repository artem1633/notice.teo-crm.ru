<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Task */
?>
<div class="task-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'petition_id',
            'house_id',
            'executor_id',
            'status_id',
            'execute_datetime',
            'description:ntext',
            'description_short:ntext',
            'priority',
            'is_fee',
            'company_id',
            'created_at',
            'created_by',
            'child_task_id',
            'parent_task_id',
        ],
    ]) ?>

</div>
