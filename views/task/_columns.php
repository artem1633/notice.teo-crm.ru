<?php
use yii\helpers\Url;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'petition_id',
        'value' => 'petition.header',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'house_id',
        'value'=>'house.address',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'executor_id',
        'value'=>'executor.fio',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'status_id',
        'value'=>'status.name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'execute_datetime',
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'description',
    // ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'description_short',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'priority',
         'content' => function($model){
             return $model->priority ? '<i class="fa fa-check text-success" style="font-size: 16px;"></i>' : '<i class="fa fa-times text-danger" style="font-size: 16px;"></i>';
         },
         'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'is_fee',
         'content' => function($model){
            return $model->is_fee ? '<i class="fa fa-check text-success" style="font-size: 16px;"></i>' : '<i class="fa fa-times text-danger" style="font-size: 16px;"></i>';
         },
         'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
     ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'company_id',
    // ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'created_at',
         'format'=>['date', 'php:d.m.Y H:i:s'],
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'created_by',
         'value'=>'createdBy.fio',
     ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'child_task_id',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'parent_task_id',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'template' => '{update}{delete}',
        'buttons' => [
            'delete' => function ($url, $model) {
                return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Удалить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-confirm-title'=>'Вы уверены?',
                    'data-confirm-message'=>'Вы действительно хотите удалить данную запись?'
                ]);
            },
            'update' => function ($url, $model) {
                return Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Изменить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                ])."&nbsp;";
            }
        ],
    ],

];   