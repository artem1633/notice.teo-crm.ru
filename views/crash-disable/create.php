<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CrashDisable */

$this->title = 'Create Crash Disable';
$this->params['breadcrumbs'][] = ['label' => 'Crash Disables', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crash-disable-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
