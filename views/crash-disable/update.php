<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CrashDisable */

$this->title = 'Update Crash Disable: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Crash Disables', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="crash-disable-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
