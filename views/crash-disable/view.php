<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CrashDisable */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Crash Disables', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="crash-disable-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'house_id',
            'start_datetime',
            'plan_end_datetime',
            'fact_end_datetime',
            'resource',
            'status',
            'company_id',
            'created_at',
            'created_by',
        ],
    ]) ?>

</div>
