<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\House;
use app\models\CrashDisable;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\CrashDisable */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="crash-disable-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'house_id')->widget(Select2::class, [
                'data' => ArrayHelper::map(House::find()->where(['company_id' => Yii::$app->user->identity->company_id])->all(), 'id', 'address'),
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'start_datetime')->widget(DateTimePicker::class, []) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'plan_end_datetime')->widget(DateTimePicker::class, []) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'fact_end_datetime')->widget(DateTimePicker::class, []) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'resource')->widget(Select2::class, [
                'data' => CrashDisable::resourceLabels(),
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'status')->widget(Select2::class, [
                'data' => CrashDisable::statusLabels(),
            ]) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
