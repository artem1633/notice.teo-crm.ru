<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\CrashDisable;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CrashDisableSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

\johnitvn\ajaxcrud\CrudAsset::register($this);

$this->title = 'Аварийные отключения';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crash-disable-index">


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'id' => 'crud-datatable',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pjax' => true,
        'toolbar' => [
            ['content' =>
                Html::a('Создать', ['create'],
                    ['role' => 'modal-remote', 'title' => 'Создать', 'class' => 'btn btn-primary']) .
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                    ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Обновить']) .
                '{toggleData}'
            ],
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'house_id',
                'value' => function($model){
                    return $model->house->address;
                },
            ],
            'start_datetime',
            'plan_end_datetime',
            'fact_end_datetime',
            [
                'attribute' => 'resource',
                'value' => function($model){
                    return ArrayHelper::getValue(CrashDisable::resourceLabels(), $model->resource);
                },
            ],
            [
                'attribute' => 'status',
                'value' => function($model){
                    return ArrayHelper::getValue(CrashDisable::statusLabels(), $model->status);
                },
            ],
            //'company_id',
            //'created_at',
            //'created_by',

            [
                'class' => 'kartik\grid\ActionColumn',
                'template' => '{view} {update} {delete}',
                'dropdown' => false,
                'vAlign' => 'middle',
                'urlCreator' => function ($action, $model, $key, $index) {
                    return Url::to([$action, 'id' => $key]);
                },
                'viewOptions' => ['role' => 'modal-remote', 'title' => 'Просмотр', 'data-toggle' => 'tooltip'],
                'updateOptions' => ['role' => 'modal-remote', 'title' => 'Изменить', 'data-toggle' => 'tooltip'],
                'deleteOptions' => [
                    'role' => 'modal-remote',
                    'title' => 'Удалить',
                    'data-confirm' => false,
                    'data-method' => false,// for overide yii data api
                    'data-request-method' => 'post',
                    'data-toggle' => 'tooltip',
                    'data-confirm-title' => 'Подтвердите действие',
                    'data-confirm-message' => 'Вы уверены что хотите удалить этот элемент?',
                ],
            ],
        ],
        'panel' => [
            'type' => 'primary',
            'heading' => '<i class="glyphicon glyphicon-list"></i> Список пользователей. ',
            'before' => '',
            'after' =>
//                        Functions::getBulkButtonWidget() .
                '<div class="clearfix"></div>',
        ]
    ]); ?>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
