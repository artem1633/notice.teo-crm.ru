<?php

use app\models\Petition;
use app\models\Users;

//$permission = Yii::$app->user->identity->permission;
//$id = Yii::$app->user->identity->id;
?>
<aside class="main-sidebar">

    <section class="sidebar">

        <?php
        if (Users::isSuperAdmin()) {
            $items = [
                ['label' => 'Меню', 'options' => ['class' => 'header']],
                ['label' => 'Компании', 'icon' => 'industry', 'url' => ['/company']],
                ['label' => 'Отчеты', 'icon' => 'tasks', 'url' => ['/petition/report']],
                ['label' => 'Сотрудники', 'icon' => 'user', 'url' => ['/users']],
                [
                    'label' => 'Задачи',
                    'encode' => false,
                    'icon' => 'wrench',
                    'url' => ['/task']
                ],
                [
                    'label' => 'Настройки',
                    'icon' => 'gear',
                    'items' => [
                        ['label' => 'База знаний', 'icon' => 'book', 'url' => ['/knowledge-base']],
                        ['label' => 'Общие настройки', 'icon' => 'gear', 'url' => ['/settings']],
                        ['label' => 'Инструкции', 'icon' => 'file', 'url' => ['/site/instructions']],
                        ['label' => 'Неисправности', 'icon' => 'hourglass-2', 'url' => ['/trouble']],
                        ['label' => 'Скачать ошибки', 'icon' => 'download', 'url' => ['site/download']],
                    ]
                ],
//                [
//                    'label' => 'Общедомовые приборы<br>учета',
//                    'icon' => 'lightbulb-o',
//                    'encode' => false,
//                    'url' => ['/site/meters']
//                ],
                [
                    'label' => 'Выход',
                    'icon' => 'sign-out',
                    'url' => ['/site/logout'],
                    'template' => '<a href="{url}" data-method="post"><i class="fa fa-sign-out"></i> {label}</a>'
                ]

            ];
        } elseif (Users::isSuperManager()) {
            $items = [
                ['label' => 'Меню', 'options' => ['class' => 'header']],
                ['label' => 'Компании', 'icon' => 'industry', 'url' => ['/company']],
                ['label' => 'Жильцы', 'icon' => 'child', 'url' => ['/resident']],
                [
                    'label' => 'Выход',
                    'icon' => 'sign-out',
                    'url' => ['/site/logout'],
                    'template' => '<a href="{url}" data-method="post"><i class="fa fa-sign-out"></i> {label}</a>'
                ]
            ];

        } elseif (Users::isAdmin()) {
            $items = [
                ['label' => 'Меню', 'options' => ['class' => 'header']],
                [
                    'label' => 'Главная' . Petition::newPetitionsLabel(),
                    'encode' => false,
                    'icon' => 'bullhorn',
                    'url' => ['/petition']
                ],
                [
                    'label' => 'Письма' . Petition::newPetitionsLabel('email'),
                    'icon' => 'envelope',
                    'encode' => false,
                    'url' => ['/message']
                ],
                [
                    'label' => 'Звонки' . Petition::newPetitionsLabel('call'),
                    'encode' => false,
                    'icon' => 'phone',
                    'url' => ['/call']
                ],
                [
                    'label' => 'Задачи',
                    'encode' => false,
                    'icon' => 'wrench',
                    'url' => ['/task']
                ],
                ['label' => 'Жалобы', 'icon' => 'exclamation', 'url' => ['/petition/complaint']],
                ['label' => 'Архив обращений', 'icon' => 'file-archive-o', 'url' => ['/petition/archive']],
                ['label' => 'Аварийные отключения', 'icon' => 'plug', 'url' => ['/crash-disable/index']],
                [
                    'label' => 'Базы данных',
                    'icon' => 'database',
                    'items' => [
                        ['label' => 'Сотрудники', 'icon' => 'user', 'url' => ['/users']],
                        ['label' => 'Дома', 'icon' => 'home', 'url' => ['/house']],
                        ['label' => 'Помещения', 'icon' => 'map-marker', 'url' => ['/apartment']],
                        ['label' => 'Документы', 'icon' => 'folder', 'url' => ['/document']],
                        ['label' => 'Жильцы', 'icon' => 'child', 'url' => ['/resident']],
                    ]
                ],
                ['label' => 'Отчеты', 'icon' => 'tasks', 'url' => ['/petition/report']],

                [
                    'label' => 'Настройки',
                    'icon' => 'gear',
                    'items' => [
                        ['label' => 'Общие настройки', 'icon' => 'gear', 'url' => ['/settings']],
                        ['label' => 'Статусы обращений', 'icon' => 'circle', 'url' => ['/status-color']],
                        [
                            'label' => 'Импорт',
                            'icon' => 'sign-in',
                            'items' => [
                                ['label' => 'Сведений о МКД', 'icon' => 'sign-in', 'url' => ['site/import-mkd']],
                                ['label' => 'Сведений о ЛС', 'icon' => 'sign-in', 'url' => ['site/import-ls']],
                            ]
                        ],
                    ]
                ],
//                [
//                    'label' => 'Общедомовые приборы<br>учета',
//                    'icon' => 'lightbulb-o',
//                    'encode' => false,
//                    'url' => ['/site/meters']
//                ],
                [
                    'label' => 'Выход',
                    'icon' => 'sign-out',
                    'url' => ['/site/logout'],
                    'template' => '<a href="{url}" data-method="post"><i class="fa fa-sign-out"></i> {label}</a>'
                ]
            ];
        } elseif (Users::isManager()) {
            $items = [
                ['label' => 'Меню', 'options' => ['class' => 'header']],
                [
                    'label' => 'Обращения' . Petition::newPetitionsLabel(),
                    'encode' => false,
                    'icon' => 'bullhorn',
                    'url' => ['/petition']
                ],
                [
                    'label' => 'Письма' . Petition::newPetitionsLabel('email'),
                    'encode' => false,
                    'icon' => 'envelope',
                    'url' => ['/message']
                ],
                [
                    'label' => 'Звонки' . Petition::newPetitionsLabel('callx'),
                    'encode' => false,
                    'icon' => 'phone',
                    'url' => ['/call']
                ],
                ['label' => 'Жильцы', 'icon' => 'child', 'url' => ['/resident']],
                [
                    'label' => 'Выход',
                    'icon' => 'sign-out',
                    'url' => ['/site/logout'],
                    'template' => '<a href="{url}" data-method="post"><i class="fa fa-sign-out"></i> {label}</a>'
                ]
            ];
        } elseif (Users::isSpecialist()) {
            $items = [
                ['label' => 'Меню', 'options' => ['class' => 'header']],
                [
                    'label' => 'Главная' . Petition::newPetitionsLabel(),
                    'encode' => false,
                    'icon' => 'bullhorn',
                    'url' => ['/petition']
                ],
                [
                    'label' => 'Выход',
                    'icon' => 'sign-out',
                    'url' => ['/site/logout'],
                    'template' => '<a href="{url}" data-method="post"><i class="fa fa-sign-out"></i> {label}</a>'
                ]
            ];
        }

        if (isset(Yii::$app->user->identity->id)) {
            try {
                echo dmstr\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                        'items' => $items,
                    ]
                );
            } catch (Exception $e) {
                Yii::error($e->getTraceAsString(), '_error');
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
        ?>

    </section>

</aside>
