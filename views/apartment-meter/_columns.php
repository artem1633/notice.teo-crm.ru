<?php
use yii\helpers\Url;

return [
         [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'created_at',
     ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'house_id',
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'cold_water',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'hot_water',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'heat',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'gas',
    ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'electricity',
     ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'visible' => \app\models\Users::isAdmin(),
        'template' => '{update} {delete}',
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Редактировать', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить',
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Вы уверены?',
                          'data-confirm-message'=>'Подтвердите удаление показаний',
                          'data-confirm-ok'=>'Удалить',
                          'data-confirm-cancel'=>'Отмена',
        ],
    ],

];   