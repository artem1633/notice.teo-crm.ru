<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ApartmentMeter */

$this->title = 'Update Apartment Meter: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Apartment Meters', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="apartment-meter-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
