<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ApartmentMeterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Apartment Meters';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="apartment-meter-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Apartment Meter', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php
    try {
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'apartment_id',
                'cold_water',
                'hot_water',
                'heat',
                //'gas',
                //'electricity',
                //'meter_number',
                //'next_date_check',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]);
    } catch (Exception $e) {
        echo $e->getMessage();
    } ?>


</div>
