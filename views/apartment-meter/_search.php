<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\ApartmentMeterSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="apartment-meter-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'apartment_id') ?>

    <?= $form->field($model, 'cold_water') ?>

    <?= $form->field($model, 'hot_water') ?>

    <?= $form->field($model, 'heat') ?>

    <?php // echo $form->field($model, 'gas') ?>

    <?php // echo $form->field($model, 'electricity') ?>

    <?php // echo $form->field($model, 'meter_number') ?>

    <?php // echo $form->field($model, 'next_date_check') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
