<?php

/* @var $this yii\web\View */
/* @var $model app\models\HouseMeter */
?>
<div class="house-meter-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
