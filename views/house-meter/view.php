<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\HouseMeter */
?>
<div class="house-meter-view">
 
    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'house_id',
                'cold_water',
                'hot_water',
                'heat',
                'gas',
                'electricity',
            ],
        ]);
    } catch (Exception $e) {
        echo $e->getMessage();
    } ?>

</div>
