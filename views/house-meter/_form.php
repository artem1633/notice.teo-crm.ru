<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model app\models\HouseMeter */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="house-meter-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    if (!$model->isNewRecord): ?>
        <h4 class="text-right">Дата предоставления: <?= Yii::$app->formatter->asDate($model->created_at); ?></h4>
    <?php endif; ?>

    <div class="row">
        <div class="col-md-6">
            <?= $model->getAttributeLabel('cold_water') ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'cold_water')->widget(MaskedInput::class, [
                'mask' => '99999.999',
                'options' => [
                    'autocomplete' => 'off'
                ]
            ])->label(false) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $model->getAttributeLabel('hot_water') ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'hot_water')->widget(MaskedInput::class, [
                'mask' => '99999.999',
                'options' => [
                    'autocomplete' => 'off'
                ]
            ])->label(false) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $model->getAttributeLabel('heat') ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'heat')->widget(MaskedInput::class, [
                'mask' => '99999.999',
                'options' => [
                    'autocomplete' => 'off'
                ]
            ])->label(false) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $model->getAttributeLabel('gas') ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'gas')->widget(MaskedInput::class, [
                'mask' => '99999.999',
                'options' => [
                    'autocomplete' => 'off'
                ]
            ])->label(false) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $model->getAttributeLabel('electricity') ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'electricity')->widget(MaskedInput::class, [
                'mask' => '999999.9',
                'options' => [
                    'autocomplete' => 'off'
                ]
            ])->label(false) ?>
        </div>
    </div>

    <?= $form->field($model, 'house_id')->hiddenInput()->label(false) ?>


    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
                ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
