<?php

/* @var $this yii\web\View */
/* @var $model app\models\StatusColor */

?>
<div class="status-color-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
