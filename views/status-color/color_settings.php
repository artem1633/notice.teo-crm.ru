<?php

use app\models\StatusColor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $statuses app\models\Status[] */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="center">
    <div class="status-color-form">
        <div class="row">
            <div class="col-md-12">
                <h4>Цветовая дифференциация статусов</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?php $form = ActiveForm::begin(); ?>

                <?php /** @var \app\models\Status $status */
                foreach ($statuses as $status): ?>
                    <div class="form-group">
                        <div class="row">
                            <label for=""
                                   class="control-label col-md-2"><?= Yii::$app->formatter->asRaw($status->name) ?></label>
                            <div class="col-md-10">
                                <div class="col-md-2">
                                    <?php
                                    $color = (new StatusColor())->getColor($status->id);
                                    echo Html::input('color', 'status_petition[' . $status->id . ']',
                                        $color ? $color : '#ffffff'
                                        , [
                                            'class' => 'form-control',
                                        ]); ?>

                                </div>
                            </div>
                        </div>


                    </div>
                <?php endforeach; ?>


                <?php if (!Yii::$app->request->isAjax) { ?>
                    <div class="col-md-4">
                        <div class="form-group">
                            <?= Html::submitButton('Сохранить',
                                [
                                    'class' => 'btn btn-primary btn-block'
                                ]) ?>
                        </div>
                    </div>
                <?php } ?>

                <?php ActiveForm::end(); ?>
            </div>
        </div>


    </div>
</div>
