<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\StatusColor */
?>
<div class="status-color-view">

    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'status_id',
                'color',
                'company_id',
            ],
        ]);
    } catch (Exception $e) {
        Yii::error($e->getTraceAsString(), '_error');
        echo $e->getMessage();
    } ?>

</div>
