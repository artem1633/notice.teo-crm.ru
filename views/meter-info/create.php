<?php

/* @var $this yii\web\View */
/* @var $model app\models\MeterInfo */
/* @var $apartment app\models\Apartment */

?>
<div class="meter-info-create">
    <?= $this->render('_form_v_2', [
        'model' => $model,
        'apartment' => $apartment,
    ]) ?>
</div>
