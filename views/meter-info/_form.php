<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model app\models\MeterInfo */
/* @var $apartment app\models\Apartment */
/* @var $form yii\widgets\ActiveForm */
$date =  $model->created_at ?  date('Y-m-d',strtotime($model->created_at)) : date('Y-m-d', time())
?>
<?php $form = ActiveForm::begin(); ?>

<div class="meter-info-form">
    <div class="row" style="display: flex; align-items: center;">
        <div class="col-md-4">
            <?= $form->field($model, 'created_at')->input('date', [
                'value' => $date,
                'disabled' => !$model->isNewRecord,
            ]) ?>
        </div>
        <div class="col-md-6 col-md-offset-2">
            <?= Html::a('Добавить прибор учета',
                ['/meter/create', 'id' => $apartment->id, 'calling' => '/meter-info/create'], [
                    'class' => 'btn btn-info btn-block',
                    'role' => 'modal-remote',
                ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">

            <?php if ($apartment->meters): ?>
                <?php foreach ($apartment->meters as $meter): ?>
                    <div class="row">
                        <div class="col-sm-6">
                            <?= $meter->typeLabel . "<br><p style='font-size: 1rem'>№{$meter->number}&nbsp;{$meter->note}</p>" ?>
                        </div>
                        <div class="col-sm-6">
                            <?= $form->field($model, 'meters_info[' . $meter->id . ']')->widget(MaskedInput::class, [
                                'mask' => $meter->type == $meter::METER_TYPE_ELECTRICITY ? '999999.9' : '99999.999',
                                'options' => [
                                    'id' => 'value-' . $meter->id,
                                    'autocomplete' => 'off'
                                ]
                            ])->label(false) ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php else: ?>
                <?php Yii::info('Счетчики не найдены', 'test') ?>
                <h4>Не найдено ни одного прибора учета</h4>
            <?php endif; ?>

            <?php if (!Yii::$app->request->isAjax) { ?>
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
                        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
