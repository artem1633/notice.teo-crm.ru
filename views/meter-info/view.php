<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\MeterInfo */
?>
<div class="meter-info-view">
 
    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'meter_id',
                'created_at',
                'value',
            ],
        ]);
    } catch (Exception $e) {
        echo $e->getMessage();
    } ?>

</div>
