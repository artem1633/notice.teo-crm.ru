<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MeterInfo */
/* @var $apartment app\models\Apartment */
/* @var $form yii\widgets\ActiveForm */
$date = $model->created_at ? date('Y-m-d', strtotime($model->created_at)) : date('Y-m-d', time())

?>
<?php $form = ActiveForm::begin(); ?>

<div class="meter-info-form">
    <?php if ($apartment->meters): ?>
        <?php /** @var \app\models\Meter $meter */
        foreach ($apartment->meters as $meter):?>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <?= $meter->typeLabel; ?>
                            <?= $meter->note ? " ({$meter->note})" : ''; ?>
                            <div class="pull-right">
                                <?= Html::a('<span class="fa fa-close"></span>', ['/meter/delete', 'id' => $meter->id],
                                    [
                                        'class' => 'btn btn-danger btn-xs',
                                        'role' => 'modal-remote',
                                        'title' => 'Удалить счетчик',
                                        'data-confirm' => false,
                                        'data-method' => false,// for overide yii data api
                                        'data-request-method' => 'post',
                                        'data-toggle' => 'tooltip',
                                        'data-confirm-title' => 'Вы уверены?',
                                        'data-confirm-message' => 'Подтвердите удаление прибора учета',
                                        'data-confirm-ok' => 'Удалить',
                                        'data-confirm-cancel' => 'Отмена',
                                    ]) ?>
                            </div>
                        </div>
                        <div class="row" style="margin: 1rem 1rem;">
                            <div class="col-sm-3">
                                <?php
                                //                                echo $form->field($model, 'meters_info[' . $meter->id . ']')->widget(MaskedInput::class,
                                //                                    [
                                //                                        'mask' => $meter->type == $meter::METER_TYPE_ELECTRICITY ? '999999.9' : '99999.999',
                                //                                        'options' => [
                                //                                            'id' => 'value-' . $meter->id,
                                //                                            'autocomplete' => 'off'
                                //                                        ]
                                //                                    ])->label('Текущие показания')
                                echo $form->field($model, 'meters_info[' . $meter->id . ']')->
                                input('number',[
                                    'autocomplete' => 'off'
                                ])->label('Текущие показания')
                                ?>
                            </div>
                            <div class="col-sm-3">
                                <label for="">Прошлый месяц</label>
                                <?= Html::textInput('text',
                                    $model->isNewRecord ? $meter->getLastValue() : $model->getPrevValue(), [
                                        'name' => 'prev_val_' . $meter->id,
                                        'class' => 'form-control',
                                        'disabled' => true,
                                    ]); ?>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="num-and-date">Серийный номер / Дата проверки</label>
                                    <div class="input-group">
                                        <input id="num-and-date" type="text" class="form-control" disabled
                                               value="<?= $meter->date_verify ? $meter->number . ' / Пов. до ' .
                                                   Yii::$app->formatter->asDate($meter->date_verify) : $meter->number ?>">
                                        <span class="input-group-btn">
                                            <?= Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                                                [
                                                    '/meter/update',
                                                    'id' => $meter->id,
                                                    'calling' => '/meter-info/create'
                                                ], [
                                                    'class' => 'btn btn-default',
                                                    'role' => 'modal-remote',

                                                ]) ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    <?php else: ?>
        <h4>Приборы учета не найдены</h4>
    <?php endif; ?>
</div>
<?php ActiveForm::end(); ?>
