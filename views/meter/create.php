<?php

/* @var $this yii\web\View */
/* @var $model app\models\Meter */

?>
<div class="meter-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
