<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Meter */
/* @var $form yii\widgets\ActiveForm */
$date =  $model->date_verify ?  date('Y-m-d',strtotime($model->date_verify)) : date('Y-m-d', time())

?>

<div class="meter-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'type')->dropDownList($model->getTypeList(), [
            'prompt' => 'Выберите значение'
    ]) ?>

    <?= $form->field($model, 'number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'date_verify')->input('date', [
            'value' => $date,
    ]) ?>

    <?= $form->field($model, 'note')->textInput([
            'placeholder' => 'Например укажите расположение: Кухня'
    ]) ?>

    <?= $form->field($model, 'apartment_id')->hiddenInput()->label(false) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
