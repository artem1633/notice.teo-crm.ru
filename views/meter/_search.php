<?php

use johnitvn\ajaxcrud\CrudAsset;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\MeterSearch */
/* @var $form yii\widgets\ActiveForm */
CrudAsset::register($this);

?>

<div class="meter-search">

    <?php $form = ActiveForm::begin([
        'action' => ['/meter/history'],
        'method' => 'get',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-2',
//                'offset' => 'col-sm-offset-1',
                'wrapper' => 'col-sm-10',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <div class="row">
        <div class="col-sm-4">
            <?= $form->field($model, 'date_start')->input('date')->label('c') ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'date_end')->input('date')->label('по') ?>
        </div>
        <div class="col-sm-4">
            <?= Html::button('Показать',[
                    'id' => 'view-history',
                    'class' => 'btn btn-primary btn-block',
                ]) ?>
        </div>
        <?= $form->field($model, 'apartment_id')->hiddenInput()->label(false); ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$script = <<<JS
$(document).ready(function() {
    $(document).on('click', '#view-history', function() {
        var start = $('#metersearch-date_start').val();
        var end = $('#metersearch-date_end').val();
        var apartment = $('input[name="MeterSearch[apartment_id]"]').val();
        $.get(
            '/meter/history',
            {
                'MeterSearch[date_start]':start,
                'MeterSearch[date_end]':end,
                'MeterSearch[apartment_id]': apartment
            },
            function(res) {
                $('.modal-body').html(res.content);
            }
        )
    })
})
JS;

$this->registerJs($script);
