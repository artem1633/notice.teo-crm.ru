<?php

use app\models\MeterInfo;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\MeterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

CrudAsset::register($this);

//\yii\helpers\VarDumper::dump($searchModel, 10, true);

?>
<?= $this->render('_search', [
    'model' => $searchModel
]) ?>
<div class="meter-history">
    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th class="th">
                    Название
                </th>
                <th class="th">
                    Дата
                </th>
                <th class="th">
                    Показание
                </th>
            </tr>
            </thead>
            <tbody>
            <?php /** @var \app\models\Meter $meter */
            foreach ($dataProvider->getModels() as $meter):?>
                <?php $meters_info = (new MeterInfo())->getValues($meter->id, $searchModel->date_start,
                    $searchModel->date_end) ?>
                <tr>
                    <td rowspan="<?= count($meters_info) ? count($meters_info) : 1; ?>"><?= $meter->typeLabel ?></td>
                <?php
                $counter = 1;
                foreach ($meters_info as $meter_info): ?>
                    <?= $counter > 1 ? '<tr>':''; ?>
                    <td><?= Yii::$app->formatter->asDate($meter_info->created_at) ?></td>
                        <td><?= $meter_info->value ?></td>
                    </tr>
                <?php endforeach; ?>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>

<?php Modal::end(); ?>
