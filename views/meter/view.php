<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Meter */
?>
<div class="meter-view">
 
    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'apartment_id',
                'number',
                'type',
                'note:ntext',
            ],
        ]);
    } catch (Exception $e) {
        echo $e->getMessage();
    } ?>

</div>
