<?php

use app\models\Meter;
use app\models\search\MeterInfoSearch;
use kartik\grid\GridView;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class' => 'kartik\grid\ExpandRowColumn',
        'width' => '50px',
        'value' => function ($model, $key, $index, $column) {
            return GridView::ROW_COLLAPSED;
        },
        'detail' => function (Meter $model, $key, $index, $column) {
            $searchModel = new MeterInfoSearch();
            $dataProvider = $searchModel->search(['MeterInfoSearch' => ['meter_id' => $model->id]]);
            return \Yii::$app->controller->renderPartial('@app/views/meter-info/index',[
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        },
        'headerOptions' => ['class' => 'kartik-sheet-style'],
        'expandOneOnly' => true
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'apartment_id',
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'type',
        'content' => function(Meter $model){
            return $model->typeLabel;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'number',
    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'note',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'template' => '{update} {delete}',
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Удаление прибора учета',
                          'data-confirm-message'=>'Подтвердите удаление прибора учета',
                          'data-confirm-ok'=>'Удалить',
                          'data-confirm-cancel'=>'Отмена'
        ],
    ],

];   