<?php

/* @var $this yii\web\View */
/* @var $model app\models\KnowledgeBase */
?>
<div class="knowledge-base-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
