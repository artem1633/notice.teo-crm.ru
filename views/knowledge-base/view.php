<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\KnowledgeBase */
?>
<div class="knowledge-base-view">
 
    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'name',
                'content:raw',
            ],
        ]);
    } catch (Exception $e) {
        echo $e->getMessage();
    } ?>

</div>
