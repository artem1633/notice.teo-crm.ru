<?php

namespace app\controllers;

use app\models\Apartment;
use app\models\Meter;
use Yii;
use app\models\MeterInfo;
use app\models\search\MeterInfoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * MeterInfoController implements the CRUD actions for MeterInfo model.
 */
class MeterInfoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulkdelete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all MeterInfo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MeterInfoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single MeterInfo model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "MeterInfo #" . $id,
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button('Закрыть',
                        ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('Редактировать', ['update', 'id' => $id],
                        ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new MeterInfo model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @param int $id Идентификатор помещения
     * @return mixed
     * @throws \yii\db\Exception
     */
    public function actionCreate($id)
    {
        $request = Yii::$app->request;
        $model = new MeterInfo();
        $apartment = Apartment::findOne($id);
//        $model->apartment_id = $apartment->id;

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Добавление показаний" .
                        Html::a('История показаний',
                            ['/meter/history', 'MeterSearch[apartment_id]' => $model->id], [
                                'class' => 'btn btn-info pull-right',
                                'style' => 'margin-right:2rem;',
                                'role' => 'modal-remote',
                            ]).
                        Html::a('Добавить прибор учета',
                            ['/meter/create', 'id' => $apartment->id, 'calling' => '/meter-info/create'], [
                                'class' => 'btn btn-info pull-right',
                                'style' => 'margin-right:2rem;',
                                'role' => 'modal-remote',
                            ]) ,
                    'size' => 'large',
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                        'apartment' => $apartment,
                    ]),
                    'footer' => Html::button('Закрыть',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            } else {
                if ($model->load($request->post())) {
                    $rows = [];
                    foreach ($model->meters_info as $meter_id => $meter_value) {
                        $meter_value = str_replace(',', '.', $meter_value);
                        $meter_value = str_replace(' ', '', $meter_value);
                        $rows[] = [$meter_id, date('Y-m-d 00:00:00', time()), $meter_value];
                    }

                    Yii::info($rows, 'test');

                    Yii::$app->db->createCommand()
                        ->batchInsert(MeterInfo::tableName(), ['meter_id', 'created_at', 'value'], $rows)
                        ->execute();

                    return [
                        'title' => "Добавление показаний",
                        'size' => 'md',
                        'content' => '<span class="text-success">Показания добавлены</span>',
                        'footer' => Html::button('Закрыть',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                    ];
                } else {
                    return [
                        'title' => "Добавление показаний",
                        'size' => 'large',
                        'content' => $this->renderAjax('create', [
                            'model' => $model,
                            'apartment' => $apartment,
                        ]),
                        'footer' => Html::button('Закрыть',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])

                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'apartment' => $apartment,
                ]);
            }
        }

    }

    /**
     * Updates an existing MeterInfo model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $apartment = $model->apartment;

        //Хаполняем данными $model->meters_info
        /** @var Meter $meter */
        foreach ($apartment->meters as $meter) {
            Yii::info($meter->attributes, 'test');
            $model->meters_info[$meter->id] = $meter->getValueByDate($model->created_at);
        }

        Yii::info($model->meters_info, 'test');
        Yii::info($model->attributes, 'test');
        Yii::info($apartment->attributes, 'test');

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Показания приборов учета",
                    'size' => 'large',
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                        'apartment' => $apartment,
                    ]),
                    'footer' => Html::button('Закрыть',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else {
                if ($model->load($request->post())) {
                    $rows = [];
                    foreach ($model->meters_info as $meter_id => $meter_value) {
                        $meter_value = str_replace(',', '.', $meter_value);
                        $meter_value = str_replace(' ', '', $meter_value);
                        $rows[] = [$meter_id, $model->created_at, $meter_value];
                    }

                    Yii::$app->db->createCommand()
                        ->batchInsert(MeterInfo::tableName(), ['meter_id', 'created_at', 'value'], $rows)
                        ->execute();
                    return [
                        'title' => "Показания приборов учета",
                        'size' => 'md',
                        'content' => '<span class="text-success">Показания изменены</span>',
                        'footer' => Html::button('Закрыть',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                    ];
                } else {
                    return [
                        'title' => "Показания приборов учета",
                        'size' => 'large',
                        'content' => $this->renderAjax('update', [
                            'model' => $model,
                            'apartment' => $apartment,
                        ]),
                        'footer' => Html::button('Закрыть',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing MeterInfo model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete multiple existing MeterInfo model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionBulkdelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the MeterInfo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MeterInfo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MeterInfo::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
